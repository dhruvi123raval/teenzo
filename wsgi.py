import os
import sys
sys.path.append('/var/www/webroot/ROOT/')

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Teenzo.settings')

application = get_wsgi_application()
