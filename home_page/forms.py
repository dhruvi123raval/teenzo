from django import forms
from home_page.models import HomePageModel,How_it_works,IconModel

class HomePageForm(forms.ModelForm):
    class Meta:
        model = HomePageModel
        fields = ('sub_title', 'main_title', 'description','img','play_link','apple_link')

    def __init__(self, *args, **kwargs):
        super(HomePageForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

class How_it_worksForm(forms.ModelForm):
    class Meta:
        model = How_it_works
        fields = ('title', 'description','video_img','video_link')

    def __init__(self, *args, **kwargs):
        super(How_it_worksForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control' 

class IconForm(forms.ModelForm):
    class Meta:
        model = IconModel
        fields = ('icon', 'number','title')

    def __init__(self, *args, **kwargs):
        super(IconForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'                        
