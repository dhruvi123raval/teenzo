from home_page import views
from django.urls import path

urlpatterns = [
    path('update/<int:pk>', views.home_page.as_view(), name="home_page_update"),
    path('add', views.home_page_create.as_view(), name="home_page_create"),
    path('how-works/update/<int:pk>', views.How_IT_Works_Update.as_view(), name="how_it_works_update"),
    path('how-works/add',views.How_IT_Works_create.as_view(), name="how_it_works_create"),
    path('icon/add', views.IconCreate.as_view(), name='icon_add'),
    path('icon/update/<int:pk>', views.IconUpdate.as_view(), name='icon_update'),
    path('icon/list', views.IconList.as_view(), name="icon_list"),

]