from django.db import models

class HomePageModel(models.Model):
    sub_title = models.CharField(max_length=200)
    main_title = models.CharField(max_length=200)
    description = models.TextField()
    img = models.ImageField(help_text = "Image size: 227px * 453px",verbose_name="Image")
    play_link = models.URLField(verbose_name="Play URL")
    apple_link = models.URLField(verbose_name="Apple URL")

class How_it_works(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    video_img = models.ImageField(help_text = "Image size: 152px * 152px",verbose_name="Image")
    video_link = models.URLField(verbose_name="Youtube URL")

class IconModel(models.Model):
    icon = models.CharField(max_length=100,verbose_name="Icon",help_text="Note : Please use <a target='_blank' href='https://fontawesome.com/icons'>https://fontawesome.com/icons</a> to insert icons below. For Eg. use class 'fa fa-facebook' ")
    number = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    

