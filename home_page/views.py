from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic.edit import UpdateView ,CreateView
from django.views.generic.list import ListView
from home_page.models import HomePageModel,How_it_works,IconModel
from django.urls import reverse_lazy
from home_page.forms import HomePageForm,How_it_worksForm,IconForm


# Create your views here.
@method_decorator(login_required, name='dispatch')
class home_page(SuccessMessageMixin, UpdateView):
	model = HomePageModel
	form_class = HomePageForm
	template_name = 'home_page/home_page_update.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Home Page updated successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("home_page_update", kwargs={"pk": pk})
	
@method_decorator(login_required, name='dispatch')
class home_page_create(SuccessMessageMixin, CreateView):
	form_class = HomePageForm
	template_name = 'home_page/home_page_create.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Home Page created successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("home_page_update", kwargs={"pk": pk})	
	
@method_decorator(login_required, name='dispatch')
class How_IT_Works_Update(SuccessMessageMixin, UpdateView):
	model = How_it_works
	form_class = How_it_worksForm
	template_name = 'home_page/how_it_works_update.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Updated successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("how_it_works_update", kwargs={"pk": pk})

@method_decorator(login_required, name='dispatch')
class How_IT_Works_create(SuccessMessageMixin, CreateView):
	form_class = How_it_worksForm
	template_name = 'home_page/how_it_works_create.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Updated successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("how_it_works_update", kwargs={"pk": pk})		

@method_decorator(login_required,name='dispatch')
class IconCreate(SuccessMessageMixin, CreateView):
    form_class = IconForm
    template_name = 'home_page/icon_create.html'
    success_url = reverse_lazy('icon_list')
    success_message = "Icon data added successfully."

@method_decorator(login_required,name='dispatch')
class IconUpdate(SuccessMessageMixin, UpdateView):
    model = IconModel
    form_class = IconForm
    template_name = 'home_page/icon_update.html'
    success_url = reverse_lazy('icon_list')
    success_message = "Icon data updated successfully."    

@method_decorator(login_required,name='dispatch')
class IconList(ListView):
    model = IconModel
    template_name = 'home_page/icon_list.html'
    context_object_name = 'data'
