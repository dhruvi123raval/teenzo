from contact import views
from django.urls import path

urlpatterns = [
    path('update/<int:pk>', views.ContactUpdate.as_view(), name="contact_update"),
    path('add', views.ContactCreate.as_view(), name="contact_create"),
    path('list', views.List.as_view(), name="contact_list"),
    path('delete/<int:pk>', views.Delete.as_view(), name="contact_delete"),
    path('profile/<int:pk>', views.Detail.as_view(), name="contact_detail"),
    path('update/privacy/<int:pk>', views.PrivacyUpdate.as_view(), name="privacy_update"),
    path('add/privacy', views.PrivacyCreate.as_view(), name="privacy_create"),
    path('update/terms-condition/<int:pk>', views.TermsandConditionUpdate.as_view(), name="terms_condition_update"),
    path('add/terms-condition', views.TermsandConditionCreate.as_view(), name="terms_condition_create"),
    path('update/refund/<int:pk>', views.RefundUpdate.as_view(), name="refund_update"),
    path('add/refund', views.RefundCreate.as_view(), name="refund_create"),
]    