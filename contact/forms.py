from django import forms
from contact.models import ContactModel,PrivacyModel,Terms_and_conditionModel,RefundModel

class ContactForm(forms.ModelForm):
    class Meta:
        model = ContactModel
        fields = ('img','title','description','appstore_link','playstore_link')

    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control' 

class PrivacyForm(forms.ModelForm):
    class Meta:
        model = PrivacyModel
        fields = ['description']

    def __init__(self, *args, **kwargs):
        super(PrivacyForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

class Terms_and_conditionForm(forms.ModelForm):
    class Meta:
        model = Terms_and_conditionModel
        fields = ['description']

    def __init__(self, *args, **kwargs):
        super(Terms_and_conditionForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'      

class RefundForm(forms.ModelForm):
    class Meta:
        model = RefundModel
        fields = ['description']

    def __init__(self, *args, **kwargs):
        super(RefundForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'                      
              