from django.db import models

# Create your models here.
class ContactModel(models.Model):
    img = models.ImageField(help_text = "Image size: 761px * 475px",verbose_name="Image")
    title = models.CharField(max_length=200)
    description = models.TextField()
    appstore_link = models.URLField(verbose_name="Appstore URL")
    playstore_link = models.URLField(verbose_name="Playstore URL")

class FillupModel(models.Model):
    email = models.CharField(max_length=200)
    message = models.TextField()   

    class Meta:
        ordering = ['-id']

    def delete(self):
        self.isDeleted = 1
        self.save()    
 
class PrivacyModel(models.Model):
    description = models.TextField() 

class Terms_and_conditionModel(models.Model):
    description = models.TextField()  

class RefundModel(models.Model):
    description = models.TextField()       