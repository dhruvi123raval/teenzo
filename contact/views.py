from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic.edit import UpdateView ,CreateView,DeleteView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from contact.models import ContactModel,FillupModel,PrivacyModel,Terms_and_conditionModel,RefundModel
from django.urls import reverse_lazy
from contact.forms import ContactForm,PrivacyForm,Terms_and_conditionForm,RefundForm


# Create your views here.
@method_decorator(login_required, name='dispatch')
class ContactUpdate(SuccessMessageMixin, UpdateView):
	model = ContactModel
	form_class = ContactForm
	template_name = 'contact/update.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Details updated successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("contact_update", kwargs={"pk": pk})
	
@method_decorator(login_required, name='dispatch')
class ContactCreate(SuccessMessageMixin, CreateView):
	form_class = ContactForm
	template_name = 'contact/create.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Details created successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("contact_update", kwargs={"pk": pk})	
	
@method_decorator(login_required,name='dispatch')
class List(ListView):
    model = FillupModel
    template_name = 'contact/list.html'
    context_object_name = 'data'
    paginate_by = 20

    def get_queryset(self):
        queryset = FillupModel.objects.all().order_by('-id')
        return queryset

@method_decorator(login_required,name='dispatch')
class Delete(SuccessMessageMixin,DeleteView):
    model = FillupModel
    success_url = reverse_lazy('contact_list')
    template_name = "contact/delete.html"
    success_message = "Contact deleted successfully."  
    
@method_decorator(login_required, name='dispatch')
class Detail(DetailView):
    model = FillupModel
    template_name = "contact/profile.html"
    context_object_name = 'data'

    def get_context_data(self, **kwargs):
        context = super(Detail, self).get_context_data(**kwargs)
        context["detail"] = FillupModel.objects.get(id=self.kwargs['pk'])
        return context

@method_decorator(login_required, name='dispatch')
class PrivacyUpdate(SuccessMessageMixin, UpdateView):
	model = PrivacyModel
	form_class = PrivacyForm
	template_name = 'contact/privacy_update.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Details updated successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("privacy_update", kwargs={"pk": pk})
	
@method_decorator(login_required, name='dispatch')
class PrivacyCreate(SuccessMessageMixin, CreateView):
	form_class = PrivacyForm
	template_name = 'contact/privacy_create.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Details created successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("privacy_update", kwargs={"pk": pk})
	
@method_decorator(login_required, name='dispatch')
class TermsandConditionUpdate(SuccessMessageMixin, UpdateView):
	model = Terms_and_conditionModel
	form_class = Terms_and_conditionForm
	template_name = 'contact/terms_condition_update.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Details updated successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("terms_condition_update", kwargs={"pk": pk})
	
@method_decorator(login_required, name='dispatch')
class TermsandConditionCreate(SuccessMessageMixin, CreateView):
	form_class = Terms_and_conditionForm
	template_name = 'contact/terms_condition_create.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Details created successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("terms_condition_update", kwargs={"pk": pk})  

@method_decorator(login_required, name='dispatch')
class RefundUpdate(SuccessMessageMixin, UpdateView):
	model = RefundModel
	form_class = RefundForm
	template_name = 'contact/refund_update.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Details updated successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("refund_update", kwargs={"pk": pk})
	
@method_decorator(login_required, name='dispatch')
class RefundCreate(SuccessMessageMixin, CreateView):
	form_class = RefundForm
	template_name = 'contact/refund_create.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Details created successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("refund_update", kwargs={"pk": pk}) 	  