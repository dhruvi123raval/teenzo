# from pyfcm import FCMNotification
# from app_users.models import AppUsersModel
# from django.conf import settings
# import logging
# import json
# from datetime import datetime

# # logger = logging.getLogger('django')
# LOGGER = logging.getLogger('loki')

# class NotificationService:

#     def __init__(self):
#         self.NOTI_KEY = 'AAAAZZ8iKqs:APA91bEf9ek9sGrBlV5kc-VJ-oSP4x_YVc0M-L-b-xoZSPsUL1FizwI7Tf1XtYPXYJTVWw786LqIYjB12lGOemJWDFnCpNxTbJm2PkAsFbmaUvlN8dsUT96A2D32xmRiivE-Dw4P_IWq'
#         # self.push_service = FCMNotification(api_key=self.NOTI_KEY)
    
#     def get_user_fcm_id(self, userId):
#         user_data = AppUsersModel.objects.get(userId = userId,isDeleted=0)
#         return user_data.fcmToken
    
#     def all_user_fcm_id(self):
#         user_data = AppUsersModel.objects.filter(isDeleted=0).values_list('fcmToken', 'userId')
#         return user_data
    
#     def is_fcm_token_blacklisted(self,fcm_token,user_id):
#         try:
#             user = AppUsersModel.objects.get(fcmToken=fcm_token, userId=user_id, isDeleted=0)
#             if user.fcmToken == 'BLACKLISTED':
#                 return True  # FCM token is blacklisted
#             else:
#                 return False  # FCM token is not blacklisted
#         except AppUsersModel.DoesNotExist:
#             return True  # FCM token is blacklisted    
 
    
#     def notification_user(self, userId, msg_title,data_message=None):
#         push_service = FCMNotification(api_key=self.NOTI_KEY)
#         registration_id = self.get_user_fcm_id(userId)

#         print("MEssage",msg_title)
#         log_data = {"MESSAGE": msg_title}
#         LOGGER.info(json.dumps(log_data))  # Convert dictionary to JSON string

#         log_data = {"Registration ID": registration_id}
#         LOGGER.info(json.dumps(log_data))

#         # Include additional data in the data payload if provided
#         if data_message is not None:
#             data_message.update({
#                 "childId": data_message.get("childId", ""),
#                 "packageName": data_message.get("packageName", ""),
#                 "appName": data_message.get("appName", ""),
#                 "appMessage": data_message.get("appMessage", ""),
#                 "senderName": data_message.get("senderName", ""),
#                 "lastNotificationSent": data_message.get("lastNotificationSent", ""),
#             })

#         print("data message",data_message)
#         LOGGER.info(f"Sending notification with data_message: {json.dumps(data_message)}")
#         data = push_service.notify_single_device(registration_id=registration_id, message_title=msg_title,data_message=data_message)
       
#         log_data = {"Data sent for notification": data}
#         LOGGER.info(json.dumps(log_data)) 
#         return data
    
#     def notification_topic(self, msg_title,msg_body):
#         log_data = {"Inside Multiple notification"}
#         LOGGER.info(json.dumps(log_data))
#         push_service = FCMNotification(api_key=self.NOTI_KEY)

#         # Send a notification to the specified topic
#         topic_name = 'SendNotification'
#         data = push_service.notify_topic_subscribers(
#             topic_name,
#             message_title=msg_title,
#             message_body=msg_body
#         )

#         # Log the result or handle as needed
#         log_data = {"Data sent for Topic Notification": data, "Topic": topic_name}
#         LOGGER.info(json.dumps(log_data))
        
#         # registration_ids_and_user_ids = list(self.all_user_fcm_id())
#         # message = f'Number of users to notify: {len(registration_ids_and_user_ids)}'
#         # logger.info(json.dumps(message))
 
#         # for registration_id, user_id in registration_ids_and_user_ids:
#         #     log_data = {"MESSAGE for user": msg_title, "UserId": user_id}
#         #     logger.info(json.dumps(log_data))

#         #     if registration_id:
#         #         if self.is_fcm_token_blacklisted(registration_id,user_id):
#         #             message = f'FCM token {registration_id} is blacklisted for user {user_id}'
#         #             logger.info(json.dumps(message))
#         #         else:
#         #             data = push_service.notify_multiple_devices(registration_ids=[registration_id], message_title=msg_title,message_body=msg_body)
#         #             log_data = {"Data sent for User Notification": data, "UserId": user_id}
#         #             logger.info(json.dumps(log_data)) 

# notification_service = NotificationService()    

import json
import logging
from google.auth.transport.requests import Request
from google.oauth2 import service_account
from app_users.models import AppUsersModel
from django.conf import settings
import requests

LOGGER = logging.getLogger('loki')

class FCMNotificationService:
    def __init__(self):
        # Path to your service account JSON file
        # self.service_account_file = '/Users/admin/Downloads/teenzo-34a90-firebase-adminsdk-4k6qh-4c935213b5.json'
        # self.service_account_file = '/var/www/html/teenzo/teenzo-34a90-firebase-adminsdk-4k6qh-4c935213b5.json'
        self.service_account_file = '/var/www/webroot/ROOT/teenzo-34a90-firebase-adminsdk-4k6qh-4c935213b5.json'
        
        # Load the credentials from the service account file
        self.credentials = service_account.Credentials.from_service_account_file(
            self.service_account_file,
            scopes=["https://www.googleapis.com/auth/firebase.messaging"]
        )

        # Define the FCM HTTP v1 API endpoint
        self.project_id = 'teenzo-34a90'
        self.fcm_endpoint = f'https://fcm.googleapis.com/v1/projects/{self.project_id}/messages:send'

    def get_access_token(self):
        # Refresh the token if needed and return it
        if not self.credentials.valid:
            LOGGER.info(json.dumps("REFRESH TOKEN"))
            self.credentials.refresh(Request())
        return self.credentials.token    
       
    def send_notification(self, token, title, body, data=None):
        access_token = self.get_access_token()  # Get a valid access token
        message = {
            "message": {
                "token": token,
                "notification": {
                    "title": title,
                    "body": body
                },
                "data": data or {}  # Optional data payload
            }
        }

        headers = {
            'Authorization': f'Bearer {access_token}',
            'Content-Type': 'application/json; UTF-8',
        }

        response = requests.post(self.fcm_endpoint, headers=headers, data=json.dumps(message))

        if response.status_code != 200:
            raise Exception(f"Error sending message: {response.status_code} {response.text}")

        return response.json()

class NotificationService:

    def __init__(self):
        self.fcm_service = FCMNotificationService()
    
    def get_user_fcm_id(self, userId):
        user_data = AppUsersModel.objects.get(userId=userId, isDeleted=0)
        return user_data.fcmToken
    
    def notification_user(self, userId, msg_title, data_message=None):
        registration_id = self.get_user_fcm_id(userId)

        log_data = {"MESSAGE": msg_title}
        LOGGER.info(json.dumps(log_data))

        log_data = {"Registration ID": registration_id}
        LOGGER.info(json.dumps(log_data))

        # Extract the message body as a string
        body = data_message.get("appMessage", "") if data_message else ""

        if data_message is not None:
            data_message.update({
                "childId": data_message.get("childId", ""),
                "packageName": data_message.get("packageName", ""),
                "appName": data_message.get("appName", ""),
                "appMessage": data_message.get("appMessage", ""),
                "senderName": data_message.get("senderName", ""),
                "lastNotificationSent": data_message.get("lastNotificationSent", ""),
            })

        LOGGER.info(f"Sending notification with data_message: {json.dumps(data_message)}")
        data = self.fcm_service.send_notification(
            token=registration_id,
            title=msg_title,
            body=body,
            data=data_message
        )
       
        log_data = {"Data sent for notification": data}
        LOGGER.info(json.dumps(log_data)) 
        return data
    
    def notification_topic(self, msg_title, msg_body):
        # This method would need to be adapted similarly if topic messaging is required
        raise NotImplementedError("Topic messaging not implemented yet in custom service")

# Usage Example:
notification_service = NotificationService()
