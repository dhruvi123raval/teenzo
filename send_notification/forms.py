from django import forms
from send_notification.models import AllUserNotificationModel


class AllUserNotificationForm(forms.ModelForm):
    class Meta:
        model = AllUserNotificationModel
        fields = ('title', 'notification_desc')

    def __init__(self, *args, **kwargs):
        super(AllUserNotificationForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'