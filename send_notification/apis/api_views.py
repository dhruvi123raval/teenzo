from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import api_view,authentication_classes,permission_classes
from rest_framework.response import Response
from rest_framework import status
from app_users.app_user_db_utils import app_user_db_utils
from app_users.models import AppUsersModel
from app_list.models import ParentChildPairModel,AppMasterModel,AppListModel
from send_notification.models import NotificationModel
from django.http import JsonResponse
from rest_framework_simplejwt.tokens import RefreshToken, AccessToken
from rest_framework_simplejwt.exceptions import TokenError
from send_notification.NotificationService import notification_service
from django.utils import timezone
from django.db.models import Count, Max
import logging
from datetime import datetime
import pytz
import re
import json

# logger = logging.getLogger(__name__)
LOGGER = logging.getLogger('loki')

@swagger_auto_schema(method='post', operation_description="send notification", tags=['notification'], request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'appName': openapi.Schema(type=openapi.TYPE_STRING, description='App name'),
        'packageName': openapi.Schema(type=openapi.TYPE_STRING, description='Package Name'),
        'appMessage': openapi.Schema(type=openapi.TYPE_STRING, description='Message'),
        'senderName': openapi.Schema(type=openapi.TYPE_STRING, description='Sender Group Name'),
    }
))
@api_view(['POST'])
@authentication_classes([])
@permission_classes([]) 
def notification_send(request):
    token_key = request.headers.get('Authorization')  
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()

        accessToken = AccessToken(token)

        userId = accessToken.payload.get('userId')
        try:
            data = ParentChildPairModel.objects.get(childId=userId,connectionStatus=1,isDeleted=0)
            check_data = AppListModel.objects.get(userId=userId,app__packageName=request.data['packageName'],isSelected=0)
            
            if check_data:
                # Create a new record
                if request.data['packageName'] == "com.whatsapp" or request.data['packageName'] == "com.whatsapp.w4b":
                    group_and_sender = re.split(r'\s*:\s*', request.data['senderName'], 1)
                    if len(group_and_sender) == 2:
                        group_name_raw, sender_name = group_and_sender

                        # Extracting the message count from group name, e.g., 'Marketing Pan India #2 (56 messages)'
                        match = re.match(r'(.+)\((\d+)\s+messages\)', group_name_raw)
                        if match:
                            group_name, message_count = match.groups()
                        else:
                            # If the pattern doesn't match, use the entire group name_raw as group name
                            group_name = group_name_raw.strip()
                            message_count = None
                    else:
                        print("Splitting failed")
                        group_name = ''
                        sender_name = request.data['senderName'].strip()
                        message_count = None

                    notification = NotificationModel(
                        appName=request.data['appName'],
                        packageName=request.data['packageName'],
                        appMessage=request.data['appMessage'],
                        senderName=sender_name.strip(),
                        groupName=group_name.strip(),
                        lastNotificationSent=timezone.now(),
                        parentId=data.parentId,
                        childId=userId
                    )
                    notification.save()
                    # split_result = str(request.data['senderName']).rsplit("):", 1)
                    # if len(split_result) == 2:
                    #     group_and_sender = split_result[0].strip()
                    #     result = split_result[1].split(":")

                    #     # Split the group name
                    #     group_split = group_and_sender.rsplit("(", 1)

                    #     # Check if the split was successful
                    #     if len(group_split) == 2:
                    #         group_name = group_split[0].strip()
                    #         sender_name = result[-1].strip()  # Use the last part after ":" as sender name
                    #     else:
                    #         print("Group name splitting failed")

                    #     notification = NotificationModel(
                    #         appName=request.data['appName'],
                    #         packageName=request.data['packageName'],
                    #         appMessage=request.data['appMessage'],
                    #         senderName=sender_name,
                    #         groupName=group_name,
                    #         lastNotificationSent=timezone.now(),
                    #         parentId=data.parentId,
                    #         childId=userId
                    #     )
                    #     notification.save()
                    #     logger.info(json.dumps("Whatsup split")) 
                    
                    # else:
                    #     print(split_result[0])
                    #     print("Splitting failed") 
                    #     logger.info(json.dumps("Whatsup split failed")) 

                    #     notification = NotificationModel(
                    #         appName=request.data['appName'],
                    #         packageName=request.data['packageName'],
                    #         appMessage=request.data['appMessage'],
                    #         senderName=split_result[0],
                    #         groupName = '',
                    #         lastNotificationSent=timezone.now(),
                    #         parentId=data.parentId,
                    #         childId=userId 
                    #     )
                    #     notification.save()
                    #     logger.info(json.dumps("Whatsup without split")) 
    
                elif request.data['packageName'] == "com.skype.raider":
                    split_result = str(request.data['senderName']).rsplit("):", 1)
                    if len(split_result) == 2:
                        group_and_sender = split_result[0].strip()
                        result = split_result[1].split(":")

                        # Split the group name
                        group_split = group_and_sender.rsplit("(", 1)

                        # Check if the split was successful
                        if len(group_split) == 2:
                            group_name = group_split[0].strip()
                            sender_name = result[-1].strip()
                        else:
                            print("Group name splitting failed")

                        notification = NotificationModel(
                            appName=request.data['appName'],
                            packageName=request.data['packageName'],
                            appMessage=request.data['appMessage'],
                            senderName=sender_name,
                            groupName = group_name,
                            lastNotificationSent=timezone.now(),
                            parentId=data.parentId,
                            childId=userId 
                        )
                        notification.save()                        
                    else:
                        print(split_result[0])
                        print("Splitting failed") 
                        notification = NotificationModel(
                            appName=request.data['appName'],
                            packageName=request.data['packageName'],
                            appMessage=request.data['appMessage'],
                            senderName=split_result[0],
                            groupName = '',
                            lastNotificationSent=timezone.now(),
                            parentId=data.parentId,
                            childId=userId 
                        )
                        notification.save()
                elif request.data['packageName'] == "com.android.mms":
                    appMessage = request.data['appMessage']
                    packageName = request.data['packageName']
                    
                    # Define regular expressions for both formats
                    match1 = re.match(r'^(\d+)\s+messages\s+\|\s+(.*)$', appMessage)
                    match2 = re.match(r'\[\d+\s+unread\](.*)', appMessage)
                    if match1:
                        _, text_message = match1.groups()
                    elif match2:
                        text_message = match2.group(1).strip()
                    else:
                        text_message = appMessage

                    notification = NotificationModel(
                        appName=request.data['appName'],
                        packageName=packageName,
                        appMessage=text_message,
                        senderName=request.data['senderName'],
                        groupName='',
                        lastNotificationSent=timezone.now(),
                        parentId=data.parentId,
                        childId=userId
                    )
                    notification.save()    
                    
                    # Assuming the message format is like "3 messages | Lhckygxxkyxxjxjxjxkckcvg"
                    # match = re.match(r'^(\d+)\s+messages\s+\|\s+(.*)$', request.data['appMessage'])
                    # if match:
                    #     num_messages, text_message = match.groups()
                    #     # Save the text_message
                    #     notification = NotificationModel(
                    #         appName=request.data['appName'],
                    #         packageName=request.data['packageName'],
                    #         appMessage=text_message.strip(),
                    #         senderName=request.data['senderName'],
                    #         groupName='',
                    #         lastNotificationSent=timezone.now(),
                    #         parentId=data.parentId,
                    #         childId=userId
                    #     )
                    #     notification.save() 
                    # else:
                    #     text_message = request.data['appMessage']
                    #     notification = NotificationModel(
                    #         appName=request.data['appName'],
                    #         packageName=request.data['packageName'],
                    #         appMessage=text_message.strip(),
                    #         senderName=request.data['senderName'],
                    #         groupName='',
                    #         lastNotificationSent=timezone.now(),
                    #         parentId=data.parentId,
                    #         childId=userId
                    #     )
                    #     notification.save() 
                elif request.data['packageName'] == "com.oneplus.mms":
                    # Assuming the senderName format is like '2 new messages: VI'
                    sender_name_split = str(request.data['senderName']).split(":")

                    if len(sender_name_split) == 2:
                        # The second part after ':' is the sender name
                        sender_name = sender_name_split[1].strip()
                    else:
                        # If splitting fails, use the original senderName
                        sender_name = request.data['senderName'].strip()

                    notification = NotificationModel(
                        appName=request.data['appName'],
                        packageName=request.data['packageName'],
                        appMessage=request.data['appMessage'],
                        senderName=sender_name,
                        groupName='',
                        lastNotificationSent=timezone.now(),
                        parentId=data.parentId,
                        childId=userId
                    )
                    notification.save()        
                elif request.data['packageName'] == "com.instagram.android":
                    # Regular expression pattern to match sender name followed by a colon
                    sender_pattern = re.compile(r'^(.*?):\s*(.*)$')

                    # Extract the sender's name from the senderName field
                    match_sender_name = sender_pattern.match(request.data['senderName'])

                    if match_sender_name:
                        sender_name = match_sender_name.group(2).strip()  # Update to group 2 to get the actual sender name
                    else:
                        # If the sender's name does not follow the expected pattern in the senderName field, use the entire senderName field as sender_name
                        sender_name = request.data['senderName']

                    # Set the message
                    app_message = request.data['appMessage']

                    # Check if the message is in the format "sender_name: message"
                    match_sender_in_message = sender_pattern.match(app_message)

                    if match_sender_in_message:
                        # Update the sender_name and app_message if sender's name is found in the message
                        sender_name = match_sender_in_message.group(1).strip()
                        app_message = match_sender_in_message.group(2).strip()
                    
                    notification = NotificationModel(
                        appName=request.data['appName'],
                        packageName=request.data['packageName'],
                        appMessage=app_message,
                        senderName=sender_name,
                        groupName='',
                        lastNotificationSent=timezone.now(),
                        parentId=data.parentId,
                        childId=userId
                    )
                    notification.save()
                elif request.data['packageName'] == "com.instagram.lite":
                    # Splitting the message based on colon message like "XYZ: Hii"
                    split_result = request.data['appMessage'].split(':', 1)
                    
                    if len(split_result) == 2:
                        sender_name = split_result[0].strip()
                        text_message = split_result[1].strip()

                        notification = NotificationModel(
                            appName=request.data['appName'],
                            packageName=request.data['packageName'],
                            appMessage=text_message,
                            senderName=request.data['senderName'],
                            groupName='',
                            lastNotificationSent=timezone.now(),
                            parentId=data.parentId,
                            childId=userId
                        )
                        notification.save()
                    else:

                        notification = NotificationModel(
                            appName=request.data['appName'],
                            packageName=request.data['packageName'],
                            appMessage=request.data['appMessage'],
                            senderName=request.data['senderName'],
                            groupName = '',
                            lastNotificationSent=timezone.now(),
                            parentId=data.parentId,
                            childId=userId 
                        )
                        notification.save()    
                elif request.data['packageName'] == "com.facebook.katana":
                    # Assuming the message format is like 'messaged you: "Hii".'
                    # Define the regular expression pattern
                    pattern = r'messaged you:\s+"([^"]+)"'

                    # Use re.search to find the match
                    match = re.search(pattern,request.data['appMessage'])
                    
                    if match:
                        text_message = match.group(1)

                        notification = NotificationModel(
                            appName=request.data['appName'],
                            packageName=request.data['packageName'],
                            appMessage=text_message,
                            senderName=request.data['senderName'],
                            groupName='',
                            lastNotificationSent=timezone.now(),
                            parentId=data.parentId,
                            childId=userId
                        )
                        notification.save()
                    else:
                        notification = NotificationModel(
                            appName=request.data['appName'],
                            packageName=request.data['packageName'],
                            appMessage=request.data['appMessage'],
                            senderName=request.data['senderName'],
                            groupName = '',
                            lastNotificationSent=timezone.now(),
                            parentId=data.parentId,
                            childId=userId 
                        )
                        notification.save() 
                elif request.data['packageName'] == "com.facebook.orca":  
                    # Assuming the senderName format is like '[Teenzo]: Mehul Patel' and 'Gandhinagar group: Samir · Gandhinagar group '
                    sender_name_split = str(request.data['senderName']).split(":", 1)

                    if len(sender_name_split) == 2:
                        # Content before the first colon is the group name
                        group_name = sender_name_split[0].strip()

                        # Content after the first colon is the sender name
                        sender_name_dirty = sender_name_split[1].strip()

                        # Use regex to find sender name before symbols like ·
                        sender_name_match = re.match(r'^([^·:]+)', sender_name_dirty)

                        if sender_name_match:
                            sender_name = sender_name_match.group(1).strip()
                        else:
                            sender_name = sender_name_dirty
                        
                        notification = NotificationModel(
                            appName=request.data['appName'],
                            packageName=request.data['packageName'],
                            appMessage=request.data['appMessage'],
                            senderName=sender_name,
                            groupName=group_name,
                            lastNotificationSent=timezone.now(),
                            parentId=data.parentId,
                            childId=userId
                        )
                        notification.save()
                    else:

                        notification = NotificationModel(
                            appName=request.data['appName'],
                            packageName=request.data['packageName'],
                            appMessage=request.data['appMessage'],
                            senderName=str(request.data['senderName']),
                            groupName='',
                            lastNotificationSent=timezone.now(),
                            parentId=data.parentId,
                            childId=userId
                        )
                        notification.save()                          
                else:
                    notification = NotificationModel(
                            appName=request.data['appName'],
                            packageName=request.data['packageName'],
                            appMessage=request.data['appMessage'],
                            senderName=request.data['senderName'],
                            groupName = '',
                            lastNotificationSent=timezone.now(),
                            parentId=data.parentId,
                            childId=userId 
                        )
                    notification.save()
                    
                saved_notification = NotificationModel.objects.get(id=notification.id)
                # log_data = {"Notification Send": saved_notification.id}
                # logger.info(json.dumps(log_data))
                notification_service.notification_user(data.parentId,saved_notification.appMessage,
                { 
                    "childId":userId,
                    "packageName": request.data['packageName'],
                    "appName" : request.data['appName'],
                    "appMessage": saved_notification.appMessage,
                    "senderName": saved_notification.senderName,
                    "lastNotificationSent": datetime.strptime(str(saved_notification.lastNotificationSent), "%Y-%m-%d %H:%M:%S.%f%z").astimezone(pytz.utc).strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
                })
                response_data = {
                    "code": "201",
                    "message": "Sent Successfully",
                    "data": "null"
                }
                return JsonResponse(response_data,status=status.HTTP_201_CREATED)
        except ParentChildPairModel.DoesNotExist:
            # msg = f"PARENT CHILD CONNECTION ERROR: {str(e)}"
            # logger.info(json.dumps(msg))
            response_data = {
                "code": "404",
                "message": "Details not found",
                "data": "null"
            }    
            return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND) 
        except AppListModel.DoesNotExist:
            # msg = f"APP LIST NOT FOUND ERROR: {str(e)}"
            # logger.info(json.dumps(msg))
            response_data = {
                "code": "404",
                "message": "Details not found",
                "data": "null"
            }    
            return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND)
    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
     
    
@swagger_auto_schema(method='post', operation_description="Get Sender Name", tags=['notification'], request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'childId': openapi.Schema(type=openapi.TYPE_STRING, description='Child ID'),
        'packageName': openapi.Schema(type=openapi.TYPE_STRING, description='Package Name'),
    }
))
@api_view(['POST'])
@authentication_classes([])
@permission_classes([])     
def get_sendername(request):
    token_key = request.headers.get('Authorization')  
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()

        accessToken = AccessToken(token)

        userId = accessToken.payload.get('userId')
        try:
            connected_children = ParentChildPairModel.objects.filter(parentId=userId,childId=request.data['childId']).last()
           
            data = (
                NotificationModel.objects
                .filter(parentId=userId,childId=request.data['childId'], packageName=request.data['packageName'], isDeleted=0,lastNotificationSent__gte=connected_children.devicePairedTime)
                .values('senderName')
                .annotate(count=Count('senderName'), latest_datetime=Max('lastNotificationSent'))
                .filter(count__gt=0)  # Exclude entries with count 0 (if needed)
                .order_by('-latest_datetime')  # Order by latest_datetime in descending order
                .distinct()
            )

            # Convert the queryset to a list for response
            sender_names_list = [{"Sender Name": entry['senderName']} for entry in data]
            sender_names_date_list = [{"senderName": entry['senderName'],"date": entry['latest_datetime']} for entry in data]

            response_data = {
                "code": "200",
                "Sender Name": sender_names_list,
                "senderName" : sender_names_date_list
            }
            return JsonResponse(response_data, status=status.HTTP_200_OK)
        except NotificationModel.DoesNotExist:
            response_data = {
                "code": "404",
                "message": "Details not found",
                "data": "null"
            }    
            return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND) 
    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
@swagger_auto_schema(method='post', operation_description="Get Messages", tags=['notification'], request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'childId': openapi.Schema(type=openapi.TYPE_STRING, description='Child ID'),
        'senderName': openapi.Schema(type=openapi.TYPE_STRING, description='Sender Name'),
        'packageName': openapi.Schema(type=openapi.TYPE_STRING, description='Package Name'),
    }
))
@api_view(['POST'])
@authentication_classes([])
@permission_classes([])     
def get_message(request):
    token_key = request.headers.get('Authorization')  
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()

        accessToken = AccessToken(token)

        userId = accessToken.payload.get('userId')
        connected_children = ParentChildPairModel.objects.filter(parentId=userId,childId=request.data['childId']).last()
        
        msg_data = NotificationModel.objects.filter(parentId=userId,childId=request.data['childId'],senderName=request.data['senderName'],packageName=request.data['packageName'],isDeleted=0,lastNotificationSent__gte=connected_children.devicePairedTime).order_by('-lastNotificationSent')
        messages = []
        for data in msg_data:
            messages.append({
                "appName": data.appName,
                "appMessage": data.appMessage,
                "childId": data.childId,
                "groupName" : data.groupName,
                "receivedTime" : data.lastNotificationSent
            })

        response_data = {
            "code": "200",
            "data": {
                "messages": messages
            }
        }
        return JsonResponse(response_data, status=status.HTTP_200_OK) 
    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)

@swagger_auto_schema(method='post', operation_description="get child notification", tags=['notification'], request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'childId': openapi.Schema(type=openapi.TYPE_STRING, description='Child ID'),
    }
))
@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def get_notification(request):
    token_key = request.headers.get('Authorization')  
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()

        accessToken = AccessToken(token)

        userId = accessToken.payload.get('userId')
        user_data = AppUsersModel.objects.get(userId=str(userId),isDeleted=0)
        if user_data.userType == 1:
            connected_children = ParentChildPairModel.objects.filter(parentId=userId,childId=request.data['childId']).last()
            child_data = NotificationModel.objects.filter(childId=request.data['childId'],parentId=userId,isDeleted=0,lastNotificationSent__gte=connected_children.devicePairedTime,appMessage__isnull=False 
            ).values('appName').annotate(latest_notification_time=Max('lastNotificationSent'),packageName=Max('packageName')).order_by('-latest_notification_time')
            # entries with non-empty appMessage
            
            response_data = {
                "code": "200",
                "message": "Sent Successfully",
                "data": []
                
            }
            
            for child_entry in child_data:
                # child_user = AppUsersModel.objects.get(userId=child_entry.childId,isDeleted=0)
            
                # is_connected = ParentChildPairModel.objects.filter(parentId=userId, childId=child_entry.childId,connectionStatus=1).exists()
                # data = ParentChildPairModel.objects.get(parentId=userId, childId=child_entry.childId,isDeleted=0)
                # notification_data = NotificationModel.objects.filter(parentId=userId,childId=child_entry.childId,isDeleted=0)
                
                child_data_entry = {
                    "appName": child_entry['appName'],
                    "packageName": child_entry['packageName'],
                    "date":child_entry['latest_notification_time']
                }
                
                response_data["data"].append(child_data_entry)
            
            return JsonResponse(response_data)
        else:
            response_data = {
            "code": "404",
            "message": "Details not found",
            "data": "NULL"
            }
            return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND)
    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)