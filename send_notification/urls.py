from django.urls import path
from send_notification import views
from send_notification.apis import api_views

urlpatterns = [
    path('create', views.send_notification_create.as_view(), name="send_notification_create"),
    path('list/', views.send_notification_list.as_view(), name="send_notification_list"),
    path('delete/<int:pk>', views.send_notification_delete.as_view(), name="send_notification_delete"),
    path('api/send/', api_views.notification_send),
    path('api/get/', api_views.get_notification),
    path('api/sender-name/', api_views.get_sendername),
    path('api/message/', api_views.get_message)
]