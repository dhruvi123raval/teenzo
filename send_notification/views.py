from django.views.generic.edit import CreateView 
from django.views.generic.edit import DeleteView
from django.views.generic.list import ListView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from send_notification.models import AllUserNotificationModel
from send_notification.forms import AllUserNotificationForm
from threading import Thread
from send_notification.NotificationService import notification_service

@method_decorator(login_required, name='dispatch')
class send_notification_create(SuccessMessageMixin, CreateView):
    form_class = AllUserNotificationForm
    template_name = 'send_notification/add.html' 
    success_url = reverse_lazy('send_notification_list')
    success_message = "Notification added successfully."

    def form_valid(self, form):
        self.object = form.save()
        t1 = Thread(target=notification_service.notification_topic, args=(self.object.title,self.object.notification_desc))
        t1.start()
        return super().form_valid(form)


@method_decorator(login_required, name='dispatch')
class send_notification_list(ListView):
    model = AllUserNotificationModel
    template_name = 'send_notification/list.html'
    context_object_name = 'data'


@method_decorator(login_required, name='dispatch')
class send_notification_delete(DeleteView):
    model = AllUserNotificationModel
    success_url = reverse_lazy('send_notification_list')
    template_name = "send_notification/delete.html"
    success_message = "Notification deleted successfully."