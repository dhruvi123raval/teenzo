from django.db import models

# Create your models here.
class NotificationModel(models.Model):
    deleteStatus = (
        (0, 'Not deleted'),
        (1, 'deleted')
    )
    appName = models.CharField(max_length=100, verbose_name="App Name")
    packageName = models.CharField(max_length=100, verbose_name="Package Name")
    appMessage = models.CharField(max_length=1500, verbose_name="Message")
    senderName = models.CharField(max_length=100, verbose_name="Sender Name")
    groupName = models.CharField(max_length=500, verbose_name="Group Name",default="")
    lastNotificationSent = models.DateTimeField(auto_now_add=True)
    parentId = models.CharField(max_length=100, verbose_name="Parent ID")
    childId = models.CharField(max_length=100, verbose_name="Child ID",default='')
    isDeleted = models.SmallIntegerField(choices=deleteStatus,default=0)

    def delete(self):
        self.isDeleted = 1
        self.save()

class AllUserNotificationModel(models.Model):

    title = models.CharField(max_length=200)
    notification_desc = models.CharField(max_length=400, verbose_name="Description")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-id']        