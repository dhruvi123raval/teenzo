from child_location import views
from django.urls import path
from child_location.apis import api_views

urlpatterns = [
    path('update/lat-long', api_views.child_lat_long, name="lat_long"),
    path('get', api_views.get_location, name="get_location"),
    path('update/time/<int:pk>', views.LatLongTimeUpdate.as_view(), name="time_update"),
    path('add/time', views.LatLongTimeCreate.as_view(), name="time_create"),
]  