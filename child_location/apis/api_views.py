from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import api_view,authentication_classes,permission_classes
from rest_framework.response import Response
from rest_framework import status
from django.http import JsonResponse
from django.utils import timezone
from rest_framework_simplejwt.tokens import RefreshToken, AccessToken
from rest_framework_simplejwt.exceptions import TokenError
from child_location.models import ChildLatLongModel
from datetime import datetime,timedelta
from django.db.models import Q
import json
import logging
# logger = logging.getLogger('django')
LOGGER = logging.getLogger('loki')

@swagger_auto_schema(method='post', operation_description="refresh token", tags=['location'], request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'lat': openapi.Schema(type=openapi.TYPE_STRING, description='Latitude'),
        'long': openapi.Schema(type=openapi.TYPE_STRING, description='Longitude'),
        'childId': openapi.Schema(type=openapi.TYPE_STRING, description='Child ID'),
        'accuracy': openapi.Schema(type=openapi.TYPE_STRING, description='Accuracy'),
    }
))
@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def child_lat_long(request):
    token_key = request.headers.get('Authorization')   
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()

        accessToken = AccessToken(token)

        userId = accessToken.payload.get('userId')
    
        data = ChildLatLongModel(
            lat=request.data['lat'],
            long=request.data['long'],
            childId=request.data['childId'],
            accuracy=request.data['accuracy'],
            createdBy=userId,
            updatedBy=userId
        )
        data.save()    

        response_data = {
            "code": "201",
            "message": "Sent Successfully",
            "data": "null"
        }

        return JsonResponse(response_data,status=status.HTTP_201_CREATED)
    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
@swagger_auto_schema(method='post', operation_description="refresh token", tags=['location'], request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'date': openapi.Schema(type=openapi.TYPE_STRING, description='Date'),
        'timezone': openapi.Schema(type=openapi.TYPE_STRING, description='Timezone'),
        'childId': openapi.Schema(type=openapi.TYPE_STRING, description='Child ID'),
    }
))
@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def get_location(request):
    token_key = request.headers.get('Authorization')   
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()

        accessToken = AccessToken(token)

        userId = accessToken.payload.get('userId')

        # Parse the date string to a datetime object
        date_str = request.data['date']
        date_obj = datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S.%f")

        # Calculate the date 7 days ago from today
        seven_days_ago = datetime.now() - timedelta(days=7)
        current_date = date_obj

        # Loop through the dates from entered date to the date 7 days ago
        while current_date >= seven_days_ago:
            lat_long = ChildLatLongModel.objects.filter(
                createdAt__date=current_date.date(),
                childId=request.data['childId'],
                isDeleted=0
            ).order_by('createdAt')

            if lat_long.exists():
                # Data found, build response and return
                location_data = [{
                    "lat": data.lat,
                    "long": data.long
                } for data in lat_long]

                response_data = {
                    "code": "201",
                    "message": "Sent Successfully",
                    "data": {
                        "locations": location_data
                    }
                }
                log_data = {"Response data for DB": response_data}
                LOGGER.info(json.dumps(log_data))
                return JsonResponse(response_data, status=status.HTTP_201_CREATED)

            # Move to the previous date
            current_date -= timedelta(days=1)

        # If no data found for the last 7 days, return Ahmedabad lat long
        default_location_data = [{"lat": "23.0225", "long": "72.5714"},{"lat": "23.0225", "long": "72.5714"},{"lat": "23.0225", "long": "72.5714"},{"lat": "23.0225", "long": "72.5714"}]
        response_data = {
            "code": "201",
            "message": "Sent Successfully",
            "data": {
                "locations": default_location_data
            }
        }
        log_data = {"Response data for Default": response_data}
        LOGGER.info(json.dumps(log_data))
        return JsonResponse(response_data, status=status.HTTP_201_CREATED)
    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)    