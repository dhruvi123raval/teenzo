from django import forms
from child_location.models import LatLongTimeUpdateModel

class LatLongTimeUpdateForm(forms.ModelForm):
    class Meta:
        model = LatLongTimeUpdateModel
        fields = ['locUploadTime']

    def __init__(self, *args, **kwargs):
        super(LatLongTimeUpdateForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control' 