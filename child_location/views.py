from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic.edit import UpdateView ,CreateView,DeleteView
from child_location.models import LatLongTimeUpdateModel
from django.urls import reverse_lazy
from child_location.forms import LatLongTimeUpdateForm

# Create your views here.
@method_decorator(login_required, name='dispatch')
class LatLongTimeUpdate(SuccessMessageMixin, UpdateView):
	model = LatLongTimeUpdateModel
	form_class = LatLongTimeUpdateForm
	template_name = 'child_location/time_update.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Details updated successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("time_update", kwargs={"pk": pk})
	
@method_decorator(login_required, name='dispatch')
class LatLongTimeCreate(SuccessMessageMixin, CreateView):
	form_class = LatLongTimeUpdateForm
	template_name = 'child_location/time_create.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Details created successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("time_update", kwargs={"pk": pk})