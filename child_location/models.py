from django.db import models
from django.utils import timezone
from django.utils.timezone import make_aware
import pytz

# Create your models here.
class ChildLatLongModel(models.Model):
    deleteStatus = (
        (0, 'Not deleted'),
        (1, 'deleted')
    )
    lat = models.CharField(max_length=100, verbose_name="Latitude")
    long = models.CharField(max_length=100, verbose_name="Longitude")
    accuracy = models.CharField(max_length=150, verbose_name="Accuracy",default='',blank=True)
    createdAt = models.DateTimeField(null=True) 
    updatedAt = models.DateTimeField(auto_now=True)
    createdBy = models.CharField(max_length=100,default='')
    updatedBy = models.CharField(max_length=100,default='')
    isDeleted = models.SmallIntegerField(choices=deleteStatus,default=0)
   
    childId = models.CharField(max_length=100, verbose_name="Child ID")

    def save(self, *args, **kwargs):
        try:
            if not self.createdAt:
                current_time_utc = timezone.now()
                ist_timezone = pytz.timezone('Asia/Kolkata')
                current_time_ist = current_time_utc.astimezone(ist_timezone)

                # Explicitly set timezone before saving
                self.createdAt = current_time_ist.replace(tzinfo=None)

            super().save(*args, **kwargs)
            print("After super().save()")
        except Exception as e:
            print("Exception during save:", e)
            raise

    def delete(self):
        self.isDeleted = 1
        self.save()

class LatLongTimeUpdateModel(models.Model):       
    locUploadTime = models.CharField(max_length=100, verbose_name="Location Upload Time") 