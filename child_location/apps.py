from django.apps import AppConfig


class ChildLocationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'child_location'
