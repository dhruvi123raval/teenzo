from django.urls import path
from app_qrcode import views
# from app_users.apis import api_views

urlpatterns = [
    # path('create', views.Create.as_view(), name="app_qrcode_create"),
    path('list/', views.List.as_view(), name="app_qrcode_list"),
    # path('update/<int:pk>', views.Update.as_view(), name="app_qrcode_update"),
    # path('delete/<int:pk>', views.Delete.as_view(), name="app_qrcode_delete"),
    # path('refresh-token/', api_views.refresh_token, name='refresh_token'),
    # path('api/signup/', api_views.app_user_register),
    # path('api/login/', api_views.check_user),
    # path('api/parent-child/', api_views.update_parent_child),
    # path('api/dashboard/parent', api_views.parent_dashboard),
    # path('api/dashboard/child', api_views.child_dashboard),
    # path('api/generate-code', api_views.pairing_code),
    # path('api/subscription', api_views.payment),
    path('app-qr-autocomplete/', views.QrAutocomplete, name='app_qr_autocomplete'),
]