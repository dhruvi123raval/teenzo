from django.db import models
from app_users.models import AppUsersModel

class AppQrcodeModel(models.Model):
    def get_user_choices():
        try:
            return [(user.userId, f"{user.id}{user.firstName}{user.lastName}") for user in AppUsersModel.objects.all()]
        except:
            ["none","none"]
    userId = models.CharField(max_length=100, verbose_name="User ID", choices=get_user_choices())
    userName = models.CharField(max_length=250, verbose_name="User Name")
    qrCodeText = models.CharField(max_length=3000, verbose_name="QR Code Text")
    qrCode = models.ImageField(upload_to='assets/qrcode/',verbose_name="qrcode")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)