from django import forms
from app_qrcode.models import AppQrcodeModel


class AppQrcodeForm(forms.ModelForm):

    class Meta:
        model = AppQrcodeModel
        fields = ('qrCode','userId','userName','qrCodeText')

    def __init__(self, *args, **kwargs):
        super(AppQrcodeForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

class AppQrcodeFormCreate(forms.ModelForm):

    class Meta:
        model = AppQrcodeModel
        fields = ('qrCodeText','userId')

    def __init__(self, *args, **kwargs):
        super(AppQrcodeFormCreate, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'