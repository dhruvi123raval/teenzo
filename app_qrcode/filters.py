import django_filters
from django import forms
from app_qrcode.models import AppQrcodeModel


class QRNameFilter(django_filters.FilterSet):
    app_user = django_filters.CharFilter(
        field_name='userName',
        lookup_expr='icontains', 
        label="User Name",
        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter username'})
    )

    class Meta:
        model = AppQrcodeModel
        fields = ['userName']

    def __init__(self, *args, **kwargs):
        super(QRNameFilter, self).__init__(*args, **kwargs)
        self.filters['userName'].label="User Name"
        self.filters['userName'].field.widget.attrs.update({'class': 'form-control'})