from io import BytesIO
from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic.edit import CreateView
from django.views.generic.edit import DeleteView
from django.views.generic.list import ListView
from django.views.generic.edit import UpdateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy,reverse
from app_qrcode.models import AppQrcodeModel
from app_qrcode.forms import AppQrcodeForm,AppQrcodeFormCreate
# from app_users.forms import AppUsersForm
from app_users.models import AppUsersModel
import qrcode
from django.core.files.base import ContentFile
from PIL import Image
import os
from django.conf import settings
import uuid
from app_qrcode.filters import QRNameFilter
from django_filters.views import FilterView
from django.http import JsonResponse

def qrcode_generate(qr_code_text,firstName_get,lastName_get,userid_get):
        # Get the qrCodeText from the form
        AppQrcodeModel.objects.filter(userId=userid_get).delete()
        existing_object = False

        if existing_object:
            # If the object already exists, display an error message
            exist_userid = existing_object.userId
            return exist_userid
        else:
            username = firstName_get+' '+lastName_get

            img = qrcode.make(qr_code_text, box_size=20)
            # Save the image to a BytesIO buffer
            buffer = BytesIO()
            img.save(buffer, format="PNG")
            buffer.seek(0)

            # Create a new entry
            new_entry = AppQrcodeModel(userId=userid_get, userName=username, qrCodeText=qr_code_text)

            # Save the image to the qrCode field
            new_entry.qrCode.save('qrcode.png', ContentFile(buffer.read()))
            new_entry.save()
            return new_entry.qrCode.url
       
# @method_decorator(login_required, name='dispatch')
# class Create(SuccessMessageMixin, CreateView):
#     model = AppQrcodeModel
#     form_class = AppQrcodeFormCreate
#     template_name = 'app_qrcode/create.html'
#     success_url = reverse_lazy('app_qrcode_list')
#     success_message = "App QR Code created successfully."
    
#     def form_valid(self, form):
#         # Get the qrCodeText from the form
#         qr_code_text = form.cleaned_data.get('qrCodeText')
#         existing_object = AppQrcodeModel.objects.filter(qrCodeText=qr_code_text).first()

#         if existing_object:
#             # If the object already exists, display an error message
#             form.add_error('qrCodeText', 'An entry with this QR code already exists.')
#             return self.form_invalid(form)
#         else:
#             username_model = AppUsersModel.objects.get(userId=form.cleaned_data.get('userId'))
#             username = username_model.firstName+' '+username_model.lastName

#             img = qrcode.make(qr_code_text, box_size=20)
#             image_path = f'qrcode_{str(uuid.uuid4())}.png'

#             # Specify the local file path to save the QR code
#             file_path = os.path.join(settings.MEDIA_ROOT, image_path)

#             # Save the QR code image to the local file
#             img.save(file_path)
#             form.instance.userName = username

#             # Save the BytesIO buffer to the qrCode field
#             form.instance.qrCode.name = image_path
            
#             # Call the parent class's form_valid method to save the rest of the form data
#             response = super().form_valid(form)

#             return response

@method_decorator(login_required, name='dispatch')
class List(FilterView):
    model = AppQrcodeModel
    template_name = 'app_qrcode/list.html'
    context_object_name = 'data'
    filterset_class = QRNameFilter
    paginate_by = 20

    def get_queryset(self):
        queryset = AppQrcodeModel.objects.all().order_by('-id')
        return queryset
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        paginator = context['paginator']
        page = context['page_obj']

        continuous_count = (page.number - 1) * paginator.per_page
        context['continuous_count'] = continuous_count

        return context


# @method_decorator(login_required, name='dispatch')
# class Update(SuccessMessageMixin, UpdateView):
#     model = AppQrcodeModel
#     form_class = AppQrcodeFormCreate
#     template_name = 'app_qrcode/update.html'
#     success_url = reverse_lazy('app_qrcode_list')
#     success_message = "App QR Code updated successfully."

#     def form_valid(self, form):
#         # Get the qrCodeText from the form
#         qr_code_text = form.cleaned_data.get('qrCodeText')
#         existing_object = AppQrcodeModel.objects.filter(qrCodeText=qr_code_text).first()

#         if existing_object:
#             # If the object already exists, display an error message
#             form.add_error('qrCodeText', 'An entry with this QR code already exists.')
#             return self.form_invalid(form)
#         else:
#             current_ooject = AppQrcodeModel.objects.get(id=self.object.id)
#             previous_qr_code_text = current_ooject.qrCodeText
#             previous_userId = current_ooject.userId
#             update_userId = form.cleaned_data.get('userId')
#             username_model = AppUsersModel.objects.get(userId=update_userId)
#             username = username_model.firstName+' '+username_model.lastName
#             if qr_code_text != previous_qr_code_text:
#                 img = qrcode.make(qr_code_text, box_size=20)
#                 image_path = f'qrcode_{str(uuid.uuid4())}.png'

#                 # Specify the local file path to save the QR code
#                 file_path = os.path.join(settings.MEDIA_ROOT, image_path)

#                 # Save the QR code image to the local file
#                 img.save(file_path)
                

#                 # Save the BytesIO buffer to the qrCode field
#                 form.instance.qrCode.name = image_path
#             if update_userId != previous_userId:
#                     form.instance.userName = username

#             # Call the parent class's form_valid method to save the rest of the form data
#             response = super().form_valid(form)  

#             return response


# @method_decorator(login_required, name='dispatch')
# class Delete(SuccessMessageMixin,DeleteView):
#     model = AppQrcodeModel
#     template_name = "app_qrcode/delete.html"
#     success_url = reverse_lazy('app_qrcode_list')
#     success_message = "App QR Code deleted successfully."

#     def form_valid(self, form):
#         # Get the instance to be deleted
#         instance = self.get_object()

#         # Delete the associated QR code image
#         if instance.qrCode:
#             # Assuming your image field is named 'qrcode_image'
#             image_path = instance.qrCode.path
#             image_path = os.path.join(settings.MEDIA_ROOT, image_path)
#             if os.path.exists(image_path):
#                 os.remove(image_path)

#         # Call the parent class's delete method to complete the deletion
#         return super().form_valid(form)

def QrAutocomplete(request):
    if request.method == 'GET':
        qs = AppQrcodeModel.objects.filter(userName__icontains=request.GET.get('term'))
        Names = list()
        for i in qs:
            Names.append(i.userName)
        return JsonResponse(Names,safe=False)  
