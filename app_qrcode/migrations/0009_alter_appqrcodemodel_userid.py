# Generated by Django 4.2.5 on 2024-01-12 09:29

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("app_qrcode", "0008_alter_appqrcodemodel_qrcode"),
    ]

    operations = [
        migrations.AlterField(
            model_name="appqrcodemodel",
            name="userId",
            field=models.CharField(max_length=100, verbose_name="User ID"),
        ),
    ]
