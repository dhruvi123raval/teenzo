from django import forms
from testimonials.models import Testimonials,PeopleModel,AboutModel

class TestimonialsForm(forms.ModelForm):
    class Meta:
        model = Testimonials
        fields = ('title', 'description')

    def __init__(self, *args, **kwargs):
        super(TestimonialsForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

class PeopleForm(forms.ModelForm):
    class Meta:
        model = PeopleModel
        fields = ('img','name', 'description')

    def __init__(self, *args, **kwargs):
        super(PeopleForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'      

class AboutForm(forms.ModelForm):
    class Meta:
        model = AboutModel
        fields = ('title', 'description')

    def __init__(self, *args, **kwargs):
        super(AboutForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'                  