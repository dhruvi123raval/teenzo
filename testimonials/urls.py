from testimonials import views
from django.urls import path

urlpatterns = [
    path('update/<int:pk>', views.TestimonialsUpdate.as_view(), name="testimonial_update"),
    path('add/', views.TestimonialsCreate.as_view(), name="testimonial_create"),
    path('people/add', views.Create.as_view(), name='people_add'),
    path('people/update/<int:pk>', views.Update.as_view(), name='people_update'),
    path('people/list', views.List.as_view(), name="people_list"),
    path('people/delete/<int:pk>', views.Delete.as_view(), name="people_delete"),
    path('about/update/<int:pk>', views.AboutUpdate.as_view(), name="about_update"),
    path('about/add', views.AboutCreate.as_view(), name="about_create"),
]    