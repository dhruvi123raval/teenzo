from django.db import models

class Testimonials(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()

class PeopleModel(models.Model):
    img = models.ImageField(help_text = "Image size: 121px * 121px",verbose_name="Image")
    name = models.CharField(max_length=200)
    description = models.TextField()    

class AboutModel(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()    
