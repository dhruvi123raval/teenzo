from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic.edit import UpdateView ,CreateView,DeleteView
from django.views.generic.list import ListView
from testimonials.models import Testimonials,PeopleModel,AboutModel
from django.urls import reverse_lazy
from testimonials.forms import TestimonialsForm,PeopleForm,AboutForm


# Create your views here.
@method_decorator(login_required, name='dispatch')
class TestimonialsUpdate(SuccessMessageMixin, UpdateView):
	model = Testimonials
	form_class = TestimonialsForm
	template_name = 'testimonials/update.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Testimonial updated successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("testimonial_update", kwargs={"pk": pk})
	
@method_decorator(login_required, name='dispatch')
class TestimonialsCreate(SuccessMessageMixin, CreateView):
	form_class = TestimonialsForm
	template_name = 'testimonials/create.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Testimonial created successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("testimonial_update", kwargs={"pk": pk})	  

@method_decorator(login_required,name='dispatch')
class Create(SuccessMessageMixin, CreateView):
    form_class = PeopleForm
    template_name = 'testimonials/people_create.html'
    success_url = reverse_lazy('people_list')
    success_message = "People added successfully."

@method_decorator(login_required,name='dispatch')
class Update(SuccessMessageMixin, UpdateView):
    model = PeopleModel
    form_class = PeopleForm
    template_name = 'testimonials/people_update.html'
    success_url = reverse_lazy('people_list')
    success_message = "People updated successfully."    

@method_decorator(login_required,name='dispatch')
class List(ListView):
    model = PeopleModel
    template_name = 'testimonials/people_list.html'
    context_object_name = 'data'
    
@method_decorator(login_required,name='dispatch')
class Delete(SuccessMessageMixin,DeleteView):
    model = PeopleModel
    success_url = reverse_lazy('people_list')
    template_name = "testimonials/people_delete.html"
    success_message = "People deleted successfully."    	  

@method_decorator(login_required, name='dispatch')
class AboutUpdate(SuccessMessageMixin, UpdateView):
	model = AboutModel
	form_class = AboutForm
	template_name = 'testimonials/about_update.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "About us updated successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("about_update", kwargs={"pk": pk})
	
@method_decorator(login_required, name='dispatch')
class AboutCreate(SuccessMessageMixin, CreateView):
	form_class = AboutForm
	template_name = 'testimonials/about_create.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "About us created successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("about_update", kwargs={"pk": pk})