"""Teenzo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from django.shortcuts import redirect

from django.urls import re_path
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
   openapi.Info(
      title="Snippets API",
      default_version='v1',
      description="Test description",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
   url="https://teenzo.com",
)

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('', lambda request: redirect('teenzo/', permanent=False)),
    path('auth/', include('custom_auth.urls')),
    path('app-user/', include('app_users.urls')),
    path('app-walkthrough/', include('walkthrough.urls')),
    path('', include('home_website.urls')),
    path('home-page/', include('home_page.urls')),
    path('feature/', include('features.urls')),
    path('testimonial/', include('testimonials.urls')),
    path('screenshot/', include('screenshot.urls')),
    path('faqs/', include('faqs.urls')),
    path('contact/', include('contact.urls')),
    path('offer/', include('offer.urls')),
    path('app-list/',include('app_list.urls')),
    path('notification/',include('send_notification.urls')),
    path('location/',include('child_location.urls')),
    path('subscription/',include('subscriptions.urls')),
    path('qrcode/', include('app_qrcode.urls')),
    path('language/', include('language.urls')),

    path('doc/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc')

]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root = settings.STATIC_URL)