"""
Django settings for Teenzo project.

Generated by 'django-admin startproject' using Django 4.0.5.

For more information on this file, see
https://docs.djangoproject.com/en/4.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/4.0/ref/settings/
"""
import os
from pathlib import Path
from datetime import timedelta
import logging
from logging.handlers import RotatingFileHandler
from pythonjsonlogger.jsonlogger import JsonFormatter
from logging_loki import LokiHandler


# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/4.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'django-insecure-jtq@liw21@^x_t49g#syp%x551@q&!ocw9m*y135e)4stosyn#'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ["teenzo.com","15.206.147.16","*"]


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'whitenoise.runserver_nostatic',
    'custom_auth',
    'app_users',
    'walkthrough',
    'home_website',
    'home_page',
    'features',
    'testimonials',
    'screenshot',
    'faqs',
    'contact',
    'offer',
    'app_list',
    'send_notification',
    'child_location',
    'subscriptions',
    'rest_framework',
    'rest_framework_swagger',
    'rest_framework_simplejwt',
    'drf_yasg',
    'django_celery_beat',
    'django_filters',
    'app_qrcode',
    'language'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
]

ROOT_URLCONF = 'Teenzo.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'Teenzo.wsgi.application'


# Database
# https://docs.djangoproject.com/en/4.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ.get('MYSQL_DATABASE', 'Teenzo'),
        'USER': os.environ.get('MYSQL_USER', 'root'),
        'PASSWORD': os.environ.get('MYSQL_PASSWORD', 'NEQqqx12192'),
        'HOST': os.environ.get('DB_HOST', 'node212206-env-9002625.in1.cantechcloud.com'),
        'PORT': '3306',
        'OPTIONS': {
            'charset': 'utf8mb4',
        },
    }
}

# Password validation
# https://docs.djangoproject.com/en/4.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/4.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.0/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = 'static/'

# Default primary key field type
# https://docs.djangoproject.com/en/4.0/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')
MEDIA_URL = '/media/'

CSRF_TRUSTED_ORIGINS = ['http://15.206.147.16/','https://teenzo.com']

LOGIN_URL='/auth/login'
LOGIN_REDIRECT_URL='/auth/login'

SWAGGER_SETTINGS = {
    'USE_SESSION_AUTH': False,
    'SECURITY_DEFINITIONS': {
        'api_key': {
            'type': 'apiKey',
            'in': 'header',
            'name': 'Authorization'
        }
    },
}

# SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
# SECURE_SSL_REDIRECT = True

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_simplejwt.authentication.JWTAuthentication',
    ),
}

SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(days=365), # 1 year
    'REFRESH_TOKEN_LIFETIME': timedelta(days=365 + 6*30), # 1 year and 6 months
    'ALGORITHM': 'HS256',
}

# LOGGING = {
#     'version': 1,
#     'disable_existing_loggers': False,
#     'formatters': {
#         'json': {
#             '()': JsonFormatter,
#             'format': '%(asctime)s %(levelname)s %(message)s',
#         },
#     },
#     'handlers': {
#         'json_file': {
#             'level': 'INFO',
#             'class': 'logging.FileHandler',
#             'filename': os.path.join(BASE_DIR, 'teenzo_log.json'),
#             'formatter': 'json',
#         },
#     },
#     'loggers': {
#         'django': {
#             'handlers': ['json_file'],
#             'level': 'INFO',
#             'propagate': True,
#         },
#     },
# }

# # Ensure the custom JSON file exists
# custom_json_file = os.path.join(BASE_DIR, 'teenzo_log.json')
# if not os.path.exists(custom_json_file):
#     with open(custom_json_file, 'w') as file:
#         file.write('[]')  # Initialize the file with an empty JSON array if it doesn't exist

# logger = logging.getLogger('django')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '[%(asctime)s] {%(module)s} [%(levelname)s] - %(message)s',
            'datefmt': '%d-%m-%Y %H:%M:%S'
        },
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'standard',
        },
        'loki': {
            'level': 'INFO',
            'class': 'logging_loki.LokiHandler',  # Adjust the import path
            'url': "http://13.201.27.133:3100/loki/api/v1/push",
            'tags': {"app": "django", "service": 'teenzo-django'},
            'version': "1",
            'formatter': 'standard'
        },
    },
    'loggers': {
        '': {
            'handlers': ['console', 'loki'],
            'level': 'DEBUG',
            'propagate': True,
        }
    }
}


# Celery configuration
CELERY_BROKER_URL = 'redis://127.0.0.1:6379'  # Use the correct URL for your message broker
CELERY_RESULT_BACKEND = 'redis://127.0.0.1:6379'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'UTC'

CELERY_BEAT_SCHEDULER = 'django_celery_beat.schedulers:DatabaseScheduler'