from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from django.conf import settings
from celery.schedules import crontab
from celery_once import QueueOnce

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Teenzo.settings')

# create a Celery instance and configure it using the settings from Django
app = Celery('Teenzo')
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

# Load the Celery Once configuration
app.conf.ONCE = {
    'backend': 'celery_once.backends.Redis',
    'settings': {
        'url': 'redis://127.0.0.1:6379',
        'default_timeout': 60 * 60
    },
}

@app.task(bind=True,base=QueueOnce)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))

app.conf.beat_schedule = {
    #Scheduler Name
    'check_subscriptiontime': {
        'task': 'subscription_expire',
        'schedule': crontab(minute=25, hour=12),
    },
    'check_latlongtime_early': {
        'task': 'notify_parent',
        'schedule': crontab(minute=45, hour=6),
        'options': {"expires": 3600 * 3},
    },
    'check_latlongtime': {
        'task': 'notify_parent',
        'schedule': crontab(minute=30, hour=13),
        'options': {"expires": 3600 * 3},
    },
}        
