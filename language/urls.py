from django.urls import path
from language import views
from language.apis import api_views

urlpatterns = [
    path('<str:language>.json', views.language_json, name='language_json'),
    path('all', api_views.language_data, name='language-list'),
] 