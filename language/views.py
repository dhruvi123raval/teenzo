from django.http import JsonResponse
import os
import json
import codecs
from django.conf import settings

def language_json(request, language):
    # Assuming your JSON files are stored in a app named "language" within the app
    json_file_path = os.path.join(settings.BASE_DIR,'language', f'{language}.json')

    try:
        with open(json_file_path, 'r', encoding='utf-8') as json_file:
            data = json.load(json_file)
            return JsonResponse(data, safe=False, json_dumps_params={'ensure_ascii': False})
    except FileNotFoundError:
        return JsonResponse({"code": "404", "message": "File not found", "data": "null"}, status=404)