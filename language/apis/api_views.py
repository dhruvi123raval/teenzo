import json
import random,string
from django.http import JsonResponse
from rest_framework import status
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import api_view
from drf_yasg import openapi

@swagger_auto_schema(method='get', operation_description="User for get Language detail", tags=['Language'])
@api_view(['GET'])
def language_data(request):
    response_data = {
            "code": "200",
            "message": "Languages found",
            "data": {
                "languages": [
                    {
                        "lang": "en",
                        "langFile": "https://teenzo.com/language/English.json",
                        "langName": "English"
                    },
                    {
                        "lang": "hi",
                        "langFile": "https://teenzo.com/language/Hindi.json",
                        "langName": "Hindi"
                    },
                    {
                        "lang": "pt",
                        "langFile": "https://teenzo.com/language/Portugal.json",
                        "langName": "Portugal"
                    },
                    {
                        "lang": "zh",
                        "langFile": "https://teenzo.com/language/Mandarin.json",
                        "langName": "Mandarin"
                    },
                    {
                        "lang": "fr",
                        "langFile": "https://teenzo.com/language/French.json",
                        "langName": "French"
                    },
                    {
                        "lang": "es",
                        "langFile": "https://teenzo.com/language/Spanish.json",
                        "langName": "Spanish"
                    }
                ]
            }
        }

    return JsonResponse(response_data)