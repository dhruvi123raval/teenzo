from django import forms
from subscriptions.models import AvailableSubscriptions

class AvailableSubscriptionsForm(forms.ModelForm):
    class Meta:
        model = AvailableSubscriptions
        fields = ('productName','productId','price','duration')

    def __init__(self, *args, **kwargs):
        super(AvailableSubscriptionsForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'