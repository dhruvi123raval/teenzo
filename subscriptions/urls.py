from subscriptions import views
from django.urls import path
from subscriptions.apis import api_views

urlpatterns = [
    path('add', views.Create.as_view(), name='subscriptions_add'),
    path('update/<int:pk>', views.Update.as_view(), name='subscriptions_update'),
    path('list', views.List.as_view(), name="subscriptions_list"),
    path('delete/<int:pk>', views.Delete.as_view(), name="subscriptions_delete"),
    path('api/send/daily-update', api_views.send_daily_update),
    path('api/get/daily-update', api_views.get_daily_update),
    path('api/update/fcm', api_views.update_fcm),
    path('user/list', views.SubscribedUsersList.as_view(), name="subscribe_user_list"),
]    