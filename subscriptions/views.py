from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic.edit import UpdateView ,CreateView,DeleteView
from django.views.generic.list import ListView
from subscriptions.models import AvailableSubscriptions
from app_users.models import AppUsersModel
from django.urls import reverse_lazy
from subscriptions.forms import AvailableSubscriptionsForm

# Create your views here.
@method_decorator(login_required,name='dispatch')
class Create(SuccessMessageMixin, CreateView):
    form_class = AvailableSubscriptionsForm
    template_name = 'subscriptions/create.html'
    success_url = reverse_lazy('subscriptions_list')
    success_message = "Subscription added successfully."

@method_decorator(login_required,name='dispatch')
class Update(SuccessMessageMixin, UpdateView):
    model = AvailableSubscriptions
    form_class = AvailableSubscriptionsForm
    template_name = 'subscriptions/update.html'
    success_url = reverse_lazy('subscriptions_list')
    success_message = "Subscription updated successfully."    

@method_decorator(login_required,name='dispatch')
class List(ListView):
    model = AvailableSubscriptions
    template_name = 'subscriptions/list.html'
    context_object_name = 'data'

    def get_queryset(self):
        queryset = AvailableSubscriptions.objects.filter(isDeleted=0)
        return queryset
    
@method_decorator(login_required,name='dispatch')
class Delete(SuccessMessageMixin,DeleteView):
    model = AvailableSubscriptions
    success_url = reverse_lazy('subscriptions_list')
    template_name = "subscriptions/delete.html"
    success_message = "Subscription deleted successfully."  

@method_decorator(login_required,name='dispatch')
class SubscribedUsersList(ListView):
    model = AppUsersModel
    template_name = 'subscriptions/subscribed_user_list.html'
    context_object_name = 'data'
    paginate_by = 20

    def get_queryset(self):
        queryset = AppUsersModel.objects.filter(isDeleted=0,isSubscribed=1)
        return queryset    
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        paginator = context['paginator']
        page = context['page_obj']

        continuous_count = (page.number - 1) * paginator.per_page
        context['continuous_count'] = continuous_count

        return context