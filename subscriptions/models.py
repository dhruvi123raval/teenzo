from django.db import models
from django.utils import timezone

# Create your models here.
class AvailableSubscriptions(models.Model):
    deleteStatus = (
        (0, 'Not deleted'),
        (1, 'deleted')
    )
    productName = models.CharField(max_length=250, verbose_name="Product Name",default='')
    productId = models.CharField(max_length=500, verbose_name="Produce Id",default='')
    price = models.CharField(max_length=100, verbose_name="Price")
    duration = models.IntegerField(verbose_name="Duration",default=0)
    isDeleted = models.SmallIntegerField(choices=deleteStatus,default=0)

    def delete(self):
        self.isDeleted = 1
        self.save()

class AdsSubscriptions(models.Model):
    deleteStatus = (
        (0, 'Not deleted'),
        (1, 'deleted')
    )
    subscriptionStatus = (
        (0, 'Not subscribed'),
        (1, 'subscribed')
    )
    rewardStatus = (
        (0, 'Not rewarded'),
        (1, 'rewarded')
    )
    userId = models.CharField(max_length=100, verbose_name="User ID",default='')
    isSubscribed = models.SmallIntegerField(choices=subscriptionStatus,default=0)
    isRewarded = models.SmallIntegerField(choices=rewardStatus,default=0)
    isDeleted = models.SmallIntegerField(choices=deleteStatus,default=0)
    time = models.DateTimeField(auto_now_add=True)
