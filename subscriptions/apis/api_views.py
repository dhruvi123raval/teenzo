from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import api_view,authentication_classes,permission_classes
from rest_framework.response import Response
from rest_framework import status
from rest_framework_simplejwt.tokens import RefreshToken, AccessToken
from rest_framework_simplejwt.exceptions import TokenError
from datetime import datetime,timedelta
from django.http import JsonResponse
from subscriptions.models import AdsSubscriptions
from django.utils import timezone
from app_users.models import AppUsersModel
from app_qrcode.models import AppQrcodeModel

@swagger_auto_schema(method='post', operation_description="ads", tags=['Ads'],
                     request_body=openapi.Schema(
                         type=openapi.TYPE_OBJECT,
                         properties={
                            'isRewarded': openapi.Schema(type=openapi.TYPE_INTEGER, description='Reward'),
                            'isSubscribed': openapi.Schema(type=openapi.TYPE_INTEGER, description='Subscribe'),
                         }
                     ))
@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def send_daily_update(request):
    token_key = request.headers.get('Authorization')   
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()

        accessToken = AccessToken(token)

        userId = accessToken.payload.get('userId')
        isRewarded = request.data['isRewarded']
        isSubscribed = request.data['isSubscribed']
        today_start = timezone.now().date()
        # Get the existing record or create a new one
        ads_subscription, created = AdsSubscriptions.objects.get_or_create(userId=userId,time__date=today_start,defaults={'isSubscribed': isSubscribed, 'isRewarded': isRewarded})

        # Update the fields
        if not created:
            ads_subscription.isSubscribed = isSubscribed
            ads_subscription.isRewarded = isRewarded
            ads_subscription.save()

        response_data = {
            "code": "201",
            "message": "Details saved successfully",
            "data": "null"
        }

        return JsonResponse(response_data,status=status.HTTP_201_CREATED)

    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)

@swagger_auto_schema(method='get', operation_description="User for get parent detail", tags=['Ads'])
@api_view(['GET'])
@authentication_classes([])
@permission_classes([]) 
def get_daily_update(request):
    token_key = request.headers.get('Authorization')
    
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()

        accessToken = AccessToken(token)
        userId = accessToken.payload.get('userId')
        
        
        today_start = timezone.now().date()
        today_end = today_start + timezone.timedelta(days=1)

        try:
            user_subscription_status = AppUsersModel.objects.get(userId=userId,isDeleted=0)
            qrcodeStatus = user_subscription_status.qrcode_status

            try:
                # Fetch the AppQrcodeModel instance
                qrCodeModel = AppQrcodeModel.objects.get(userId=str(user_subscription_status.email))
                # Build the absolute URL for the qrCode image
                if qrcodeStatus:
                    qrCodeImage_url = request.build_absolute_uri(qrCodeModel.qrCode.url)
                    qrCodeReferralKey = qrCodeModel.qrCodeText
                else:
                    default_qrcode = '/media/default_qrcode/teenzo_qr_code.jpeg'
                    qrCodeImage_url = request.build_absolute_uri(default_qrcode)
                    qrCodeReferralKey = ''
            except:
                default_qrcode = '/media/default_qrcode/teenzo_qr_code.jpeg'
                qrCodeImage_url = request.build_absolute_uri(default_qrcode)
                qrCodeReferralKey = ''
            
            ads_data = AdsSubscriptions.objects.filter(
                userId=userId,
                isDeleted=0,
                time__date=today_start
            ).first()

            if ads_data:
                response_data = {
                    "code": "200",
                    "message": "Success",
                    "data":  {
                        "isRewarded": ads_data.isRewarded,
                        "isSubscribed": user_subscription_status.isSubscribed,
                        "qrCodeImage":qrCodeImage_url,
                        "referralKey":qrCodeReferralKey,
                        # "isTermsAndConditionAccepted":int(user_subscription_status.isTermsAndConditionAccepted)
                    }
                }
                return JsonResponse(response_data, status=status.HTTP_200_OK)
            else:
                response_data = {
                    "code": "200",
                    "message": "Success",
                    "data":  {
                        "isRewarded": 0,
                        "isSubscribed": user_subscription_status.isSubscribed,
                        "qrCodeImage":qrCodeImage_url,
                        "referralKey":qrCodeReferralKey,
                        # "isTermsAndConditionAccepted":int(user_subscription_status.isTermsAndConditionAccepted)
                    }
                }
                return JsonResponse(response_data, status=status.HTTP_200_OK)
              
        except AppUsersModel.DoesNotExist:
            response_data = {
                    "code": "404",
                    "message": "Data not found",
                    "data": "NULL"
                }
            return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND)      

    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
@swagger_auto_schema(method='post', operation_description="ads", tags=['FCM'],
                     request_body=openapi.Schema(
                         type=openapi.TYPE_OBJECT,
                         properties={
                            'fcmToken': openapi.Schema(type=openapi.TYPE_STRING, description='FCM token'),
                         }
                     ))
@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def update_fcm(request):
    token_key = request.headers.get('Authorization')   
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()

        accessToken = AccessToken(token)

        userId = accessToken.payload.get('userId')
        
        try:
            user = AppUsersModel.objects.get(userId=userId,isDeleted=0)
            user.fcmToken = request.data['fcmToken']
            user.save()
        except AppUsersModel.DoesNotExist:
            response_data = {
                "code": "404",
                "message": "User not found",
                "data": "null"
            }    
            return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND)

        response_data = {
            "code": "201",
            "message": "Details saved successfully",
            "data": "null"
        }

        return JsonResponse(response_data,status=status.HTTP_201_CREATED)

    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)    