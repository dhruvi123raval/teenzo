from django import forms
from offer.models import OfferModel

class OfferForm(forms.ModelForm):
    class Meta:
        model = OfferModel
        fields = ('title','description')

    def __init__(self, *args, **kwargs):
        super(OfferForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'