from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic.edit import UpdateView ,CreateView,DeleteView
from django.views.generic.list import ListView
from offer.models import OfferModel
from django.urls import reverse_lazy
from offer.forms import OfferForm


# Create your views here.
@method_decorator(login_required, name='dispatch')
class OfferUpdate(SuccessMessageMixin, UpdateView):
	model = OfferModel
	form_class = OfferForm
	template_name = 'offer/update.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Offer updated successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("offer_update", kwargs={"pk": pk})
	
@method_decorator(login_required, name='dispatch')
class OfferCreate(SuccessMessageMixin, CreateView):
	form_class = OfferForm
	template_name = 'offer/create.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Offer created successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("offer_update", kwargs={"pk": pk})
