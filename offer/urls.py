from offer import views
from django.urls import path

urlpatterns = [
    path('update/<int:pk>', views.OfferUpdate.as_view(), name="offer_update"),
    path('add/', views.OfferCreate.as_view(), name="offer_create"),
]    