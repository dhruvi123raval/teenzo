from walkthrough.models import WalkthroughModel
from rest_framework import serializers

class WalkthroughSerializer(serializers.ModelSerializer):
    class Meta:
        model = WalkthroughModel
        fields = ['title', 'image', 'description']
