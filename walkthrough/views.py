from django.views.generic.edit import CreateView
from django.views.generic.edit import DeleteView
from django.views.generic.list import ListView
from django.views.generic.edit import UpdateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from walkthrough.models import WalkthroughModel,FirstScreenModel,DisclaimerModel
from walkthrough.forms import WalkthroughForm,FirstScreenForm,DisclaimerForm


@method_decorator(login_required, name='dispatch')
class Create(SuccessMessageMixin, CreateView):
    form_class = WalkthroughForm
    template_name = 'walkthrough/create.html'
    success_url = reverse_lazy('walkthrough_list')
    success_message = "Walkthrough added successfully."


@method_decorator(login_required, name='dispatch')
class List(ListView):
    model = WalkthroughModel
    template_name = 'walkthrough/list.html'
    context_object_name = 'data'
    paginate_by = 20

    def get_queryset(self):
        queryset = WalkthroughModel.objects.filter(isDeleted=0).order_by('-id')
        return queryset


@method_decorator(login_required, name='dispatch')
class Update(SuccessMessageMixin, UpdateView):
    model = WalkthroughModel
    form_class = WalkthroughForm
    template_name = 'walkthrough/update.html'
    success_url = reverse_lazy('walkthrough_list')
    success_message = "Walkthrough updated successfully."


@method_decorator(login_required, name='dispatch')
class Delete(DeleteView):
    model = WalkthroughModel
    success_url = reverse_lazy('walkthrough_list')
    template_name = "walkthrough/delete.html"
    success_message = "Walkthrough deleted successfully."

@method_decorator(login_required, name='dispatch')
class CreateScreen(SuccessMessageMixin, CreateView):
    form_class = FirstScreenForm
    template_name = 'walkthrough/screen_create.html'
    success_url = reverse_lazy('screen_list')
    success_message = "Screen added successfully."

@method_decorator(login_required, name='dispatch')
class ListScreen(ListView):
    model = FirstScreenModel
    template_name = 'walkthrough/screen_list.html'
    context_object_name = 'data'
    paginate_by = 20

    def get_queryset(self):
        queryset = FirstScreenModel.objects.filter(isDeleted=0).order_by('-id')
        return queryset


@method_decorator(login_required, name='dispatch')
class UpdateScreen(SuccessMessageMixin, UpdateView):
    model = FirstScreenModel
    form_class = FirstScreenForm
    template_name = 'walkthrough/screen_update.html'
    success_url = reverse_lazy('screen_list')
    success_message = "Screen updated successfully."


@method_decorator(login_required, name='dispatch')
class DeleteScreen(DeleteView):
    model = FirstScreenModel
    success_url = reverse_lazy('screen_list')
    template_name = "walkthrough/screen_delete.html"
    success_message = "Screen deleted successfully."    

@method_decorator(login_required, name='dispatch')
class CreateDisclaimer(SuccessMessageMixin, CreateView):
    form_class = DisclaimerForm
    template_name = 'walkthrough/disclaimer_create.html'
    success_url = reverse_lazy('disclaimer_list')
    success_message = "Disclaimer added successfully."

@method_decorator(login_required, name='dispatch')
class ListDisclaimer(ListView):
    model = DisclaimerModel
    template_name = 'walkthrough/disclaimer_list.html'
    context_object_name = 'data'
    paginate_by = 20

    def get_queryset(self):
        queryset = DisclaimerModel.objects.filter(isDeleted=0).order_by('-id')
        return queryset

@method_decorator(login_required, name='dispatch')
class DeleteDisclaimer(DeleteView):
    model = DisclaimerModel
    success_url = reverse_lazy('disclaimer_list')
    template_name = "walkthrough/disclaimer_delete.html"
    success_message = "Disclaimer deleted successfully."       