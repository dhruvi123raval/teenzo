from django.urls import path
from walkthrough import views
from walkthrough.apis import api_views

urlpatterns = [
    path('add', views.Create.as_view(), name='walkthrough_add'),
    path('update/<int:pk>', views.Update.as_view(), name='walkthrough_update'),
    path('list', views.List.as_view(), name="walkthrough_list"),
    path('delete/<int:pk>', views.Delete.as_view(), name="walkthrough_delete"),

    path('screen/add', views.CreateScreen.as_view(), name='screen_add'),
    path('screen/update/<int:pk>', views.UpdateScreen.as_view(), name='screen_update'),
    path('screen/list', views.ListScreen.as_view(), name="screen_list"),
    path('screen/delete/<int:pk>', views.DeleteScreen.as_view(), name="screen_delete"),
    
    path('disclaimer/add', views.CreateDisclaimer.as_view(), name='disclaimer_add'),
    path('disclaimer/list', views.ListDisclaimer.as_view(), name="disclaimer_list"),
    path('disclaimer/delete/<int:pk>', views.DeleteDisclaimer.as_view(), name="disclaimer_delete"),

    path('api/all', api_views.walkthrouth_data, name='walkthrough-list'),
    
]
