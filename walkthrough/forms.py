from django import forms
from walkthrough.models import WalkthroughModel,FirstScreenModel,DisclaimerModel


class WalkthroughForm(forms.ModelForm):
    class Meta:
        model = WalkthroughModel
        fields = ('title', 'image', 'description')

    def __init__(self, *args, **kwargs):
        super(WalkthroughForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

class FirstScreenForm(forms.ModelForm):
    class Meta:
        model = FirstScreenModel
        fields = ('title', 'image', 'description')

    def __init__(self, *args, **kwargs):
        super(FirstScreenForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

class DisclaimerForm(forms.ModelForm):
    class Meta:
        model = DisclaimerModel
        fields = ['description']

    def __init__(self, *args, **kwargs):
        super(DisclaimerForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'