from walkthrough.models import WalkthroughModel,FirstScreenModel,DisclaimerModel,PrivateKeyModel
from walkthrough.serializers import WalkthroughSerializer
from language.models import MultiLanguageModel
from rest_framework import generics
from rest_framework.response import Response
import json
import random,string
from django.http import JsonResponse
from rest_framework import status
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import api_view
from drf_yasg import openapi


@swagger_auto_schema(method='get', operation_description="User for get walkthrough detail", tags=['Walkthrough'])
@api_view(['GET'])
def walkthrouth_data(request):
    token_key = request.headers.get('Authorization')
    
    if not token_key or token_key != "HelloWorld!":
        response_data = {
            "code": "401",
            "message": "Authorization header missing or incorrect",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    walkthrough_data = WalkthroughModel.objects.filter(isDeleted=0)
    screen_data = FirstScreenModel.objects.first()
    disclaimer_data = DisclaimerModel.objects.filter(isDeleted=0)

    if not walkthrough_data and screen_data:
        response_data = {
            "code": "404",
            "message": "Data not found",
            "data": "NULL"
        }
    else:
        walkthrough_details = []
        disclaimer_details = []
        for item in walkthrough_data:
            detail = {
                "heading": item.title,
                "imageUrl": item.image.url if item.image else "",
                "description": item.description,
            }
            walkthrough_details.append(detail)

        for i in disclaimer_data:
            detail = i.description
            disclaimer_details.append(detail)
    
        key_length = 6  # Change the length as needed
        new_private_key = ''.join(random.choices(string.ascii_letters + string.digits, k=key_length))

        private_key_obj = PrivateKeyModel(key=new_private_key)
        private_key_obj.save()    

        response_data = {
            "code": "200",
            "message": "Details found",
            "data": {
                "disclaimer" : disclaimer_details,
                "firstScreen":{
                    "heading": screen_data.title,
                    "imageUrl": screen_data.image.url if screen_data.image else "",
                    "description": screen_data.description,
                },
                "privateKey" : new_private_key,
                "walkthroughDetails": walkthrough_details
            }
        }

    return JsonResponse(response_data)