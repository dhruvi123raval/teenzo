from django.db import models

class WalkthroughModel(models.Model):
    deleteStatus = (
        (0, 'Not deleted'),
        (1, 'deleted')
    )
    title = models.CharField(max_length=100, blank=True,verbose_name="Walkthrough Title")
    image = models.ImageField(verbose_name="Walkthrough Image",
                                    help_text="Image size 70px * 70px (width*height). "
                                              "only jpg, png, jpeg format allowed")
    description = models.CharField(max_length=300, blank=True,verbose_name="Walkthrough Description")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    isDeleted = models.SmallIntegerField(choices=deleteStatus,default=0)

    def delete(self):
        self.isDeleted = 1
        self.save()

class FirstScreenModel(models.Model):
    deleteStatus = (
        (0, 'Not deleted'),
        (1, 'deleted')
    )
    title = models.CharField(max_length=100, verbose_name="Screen Title")
    image = models.ImageField(null=True, blank=True, verbose_name="Screen Image", default='default_image.jpg',
                                    help_text="Image size 70px * 70px (width*height). "
                                              "only jpg, png, jpeg format allowed")
    description = models.CharField(max_length=300, verbose_name="Screen Description")
    isDeleted = models.SmallIntegerField(choices=deleteStatus,default=0)

    def delete(self):
        self.isDeleted = 1
        self.save()

class DisclaimerModel(models.Model):
    deleteStatus = (
        (0, 'Not deleted'),
        (1, 'deleted')
    )
    description = models.CharField(max_length=300, verbose_name="Disclaimer Description")  
    isDeleted = models.SmallIntegerField(choices=deleteStatus,default=0)

    def delete(self):
        self.isDeleted = 1
        self.save() 

class PrivateKeyModel(models.Model):
    key = models.CharField(max_length=300, verbose_name="Key")
    createdAt = models.DateTimeField(auto_now_add=True)       
