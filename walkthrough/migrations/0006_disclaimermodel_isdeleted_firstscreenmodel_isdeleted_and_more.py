# Generated by Django 4.0.5 on 2023-11-03 06:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('walkthrough', '0005_rename_created_at_privatekeymodel_createdat'),
    ]

    operations = [
        migrations.AddField(
            model_name='disclaimermodel',
            name='isDeleted',
            field=models.SmallIntegerField(choices=[(0, 'Not deleted'), (1, 'deleted')], default=0),
        ),
        migrations.AddField(
            model_name='firstscreenmodel',
            name='isDeleted',
            field=models.SmallIntegerField(choices=[(0, 'Not deleted'), (1, 'deleted')], default=0),
        ),
        migrations.AddField(
            model_name='walkthroughmodel',
            name='isDeleted',
            field=models.SmallIntegerField(choices=[(0, 'Not deleted'), (1, 'deleted')], default=0),
        ),
    ]
