# Generated by Django 4.0.5 on 2023-09-18 06:00

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='WalkthroughModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100, verbose_name='Walkthrough Title')),
                ('image', models.ImageField(blank=True, default='default_image.jpg', help_text='Image size 70px * 70px (width*height). only jpg, png, jpeg format allowed', null=True, upload_to='', verbose_name='Walkthrough Image')),
                ('description', models.CharField(max_length=300, verbose_name='Walkthrough Description')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
