# Generated by Django 4.0.5 on 2023-12-01 18:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('walkthrough', '0007_alter_walkthroughmodel_description_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='walkthroughmodel',
            name='image',
            field=models.ImageField(default='default_image.jpg', help_text='Image size 70px * 70px (width*height). only jpg, png, jpeg format allowed', upload_to='', verbose_name='Walkthrough Image'),
        ),
    ]
