# Generated by Django 4.0.5 on 2023-10-20 07:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('walkthrough', '0004_privatekeymodel'),
    ]

    operations = [
        migrations.RenameField(
            model_name='privatekeymodel',
            old_name='created_at',
            new_name='createdAt',
        ),
    ]
