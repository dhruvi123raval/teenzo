from rest_framework.generics import CreateAPIView, RetrieveAPIView
from app_users.serializers import UserSerializer
from app_users.models import AppUsersModel,PairingCodeModel
from app_list.models import ParentChildPairModel,AppListModel
from app_qrcode.models import AppQrcodeModel
from app_list.serializers import AppListDisplaySerializer
from send_notification.models import NotificationModel
from send_notification.NotificationService import notification_service
from walkthrough.models import PrivateKeyModel
from child_location.models import ChildLatLongModel,LatLongTimeUpdateModel
from subscriptions.models import AvailableSubscriptions,AdsSubscriptions
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import api_view,authentication_classes,permission_classes
from rest_framework.response import Response
from rest_framework import status
from app_users.app_user_db_utils import app_user_db_utils,subscribe_user_to_topic,is_user_subscribed_to_topic
from django.http import JsonResponse
from django.core.cache import cache
from django.utils import timezone
import uuid
import jwt
import random,string
from rest_framework_simplejwt.tokens import RefreshToken, AccessToken
from rest_framework_simplejwt.exceptions import TokenError
from datetime import datetime,timedelta
import traceback
import json
import logging
from app_qrcode.views import qrcode_generate
import requests
from firebase_admin import auth, messaging
from django.core.paginator import Paginator,EmptyPage

# logger = logging.getLogger('django')
LOGGER = logging.getLogger('loki') 

class UserListCreateView(CreateAPIView):
    queryset = AppUsersModel.objects.all()
    serializer_class = UserSerializer

class UserRetrieveView(RetrieveAPIView):
    queryset = AppUsersModel.objects.all()
    serializer_class = UserSerializer    

@swagger_auto_schema(method='post', operation_description="refresh access token", tags=['app-user'], request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'refreshToken': openapi.Schema(type=openapi.TYPE_STRING, description='refresh token')
    }
))
@api_view(['POST'])
def new_access_token(request):
    refreshToken = request.data['refreshToken']

    if not refreshToken:
        response_data = {
            "code": "400",
            "message": "Refresh token is missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_400_BAD_REQUEST)

    try:
        refresh = RefreshToken(refreshToken)
        max_refresh_token_lifetime = timedelta(days=365 + 6*30)  
        cache.set(refreshToken, datetime.now(), timeout=max_refresh_token_lifetime.total_seconds())

        creation_time = cache.get(refreshToken)

        if creation_time is None:
            return JsonResponse({'code':'401','message': 'Refresh token not found or has expired, please log in again', 'data': 'null'}, status=status.HTTP_401_UNAUTHORIZED)

        if datetime.now() > (creation_time + max_refresh_token_lifetime):
            return JsonResponse({'code':'401','message': 'Refresh token has expired, please log in again','data': 'null'}, status=status.HTTP_401_UNAUTHORIZED)
        
        userId = refresh.get('userId')
        if userId is None:
            return JsonResponse({'code':'404','message':'User ID not found in the refresh token','data': 'null'},status=status.HTTP_404_NOT_FOUND)

        user = AppUsersModel.objects.get(userId=userId)
        timestamp_in_milliseconds = int(datetime.now().timestamp() * 1000)

        new_access_token = AccessToken.for_user(user)
        new_access_token.payload['userId'] = userId
        new_access_token.payload['timestamp'] = timestamp_in_milliseconds
        new_access_token.payload['email'] = user.email
        user.accessToken = new_access_token
        user.save()

        response_data = {
            "code": "200",
            "message": "Success",
            "data": {
                'accessToken': str(new_access_token)
            }
        }
        return JsonResponse(response_data, status=status.HTTP_200_OK)
    except TokenError as e:
        print("TokenError:", e)
        return JsonResponse({'code':'401','message': 'Token is Expired','data': 'null'}, status=status.HTTP_401_UNAUTHORIZED)
    except Exception as e:
        return JsonResponse({'code':'401','message': 'Invalid refresh token','data': 'null'}, status=status.HTTP_401_UNAUTHORIZED)
    
@swagger_auto_schema(method='post', operation_description="New refresh token", tags=['app-user'], request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'refreshToken': openapi.Schema(type=openapi.TYPE_STRING, description='refresh token')
    }
))
@api_view(['POST'])   
def new_refresh_token(request):
    refreshToken = request.data['refreshToken']

    if not refreshToken:
        response_data = {
            "code": "400",
            "message": "Refresh token is missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_400_BAD_REQUEST) 
    
    try:
        creation_time = cache.get(refreshToken)
        max_refresh_token_lifetime = timedelta(days=365 + 6*30)

        if creation_time is None or timezone.now() > (creation_time + max_refresh_token_lifetime):
            # If the refresh token is expired or not found, generate a new one
            
            userId = RefreshToken(refreshToken, verify=False).get('userId') # Skip verification

            if userId is None:
                return Response({'code':'404','message':'User ID not found in the refresh token','data': 'null'},status=status.HTTP_404_NOT_FOUND)

            user = AppUsersModel.objects.get(userId=userId)

            timestamp_in_milliseconds = int(datetime.now().timestamp() * 1000)
            new_refresh_token = RefreshToken.for_user(user)
            new_refresh_token.payload['userId'] = userId
            new_refresh_token.payload['timestamp'] = timestamp_in_milliseconds
            new_refresh_token.payload['email'] = user.email
            user.refreshToken = new_refresh_token
            user.save()

            # Update the cache with the new refresh token
            cache.set(str(new_refresh_token), timezone.now(), timeout=max_refresh_token_lifetime.total_seconds())

            return Response({'code':'200','message': 'New refresh token generated','refreshToken': str(new_refresh_token)}, status=status.HTTP_200_OK)
            
        # If refresh token is valid, return appropriate response
        return Response({'code':'200','message': 'Refresh token is valid','data': 'null'}, status=status.HTTP_200_OK)

    except Exception as e:
        print("Exception:", e)
        return Response({'code':'500','message': 'Internal Server Error','data': 'null'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@swagger_auto_schema(
    method='post',
    operation_description="User registration",
    tags=['app-user'],
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'email': openapi.Schema(type=openapi.TYPE_STRING, description='User email'),
            'referralKey': openapi.Schema(type=openapi.TYPE_STRING, description='Referral Key'),
            'referredBy': openapi.Schema(type=openapi.TYPE_STRING, description='Referred By'),
            'privatekey': openapi.Schema(type=openapi.TYPE_STRING, description='Private Key'),
            'returnkey': openapi.Schema(type=openapi.TYPE_STRING, description='Return Key'),
            'firstName': openapi.Schema(type=openapi.TYPE_STRING, description='User first name'),
            'lastName': openapi.Schema(type=openapi.TYPE_STRING, description='User last name'),
            'fcmToken': openapi.Schema(type=openapi.TYPE_STRING, description='FCM token'),
            'appVersion': openapi.Schema(type=openapi.TYPE_STRING, description='App version'),
            'androidVersion': openapi.Schema(type=openapi.TYPE_STRING, description='Android version'),
            'manufacturerName': openapi.Schema(type=openapi.TYPE_STRING, description='Manufacturer name'),
            'modelName': openapi.Schema(type=openapi.TYPE_STRING, description='Model name'),
            'isTermsAndConditionAccepted' : openapi.Schema(type=openapi.TYPE_INTEGER, description='Terms and condition'),
        }
    )
)
@api_view(['POST'])
def app_user_register(request):
    token_key = request.headers.get('Authorization')
    
    if not token_key or token_key != "HelloWorld!":
        response_data = {
            "code": "401",
            "message": "Authorization header missing or incorrect",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    if 'returnkey' in request.data and request.data['returnkey']:
        response_data = {
            "code": "404",
            "message": "Data not found",
            "data": "NULL"
        }
        return JsonResponse(response_data,status=status.HTTP_404_NOT_FOUND)
    
    private_key_value = request.data['privatekey']
    # Check if private key exists in PrivateKeyModel
    try:
        private_key_obj = PrivateKeyModel.objects.get(key=private_key_value)
    except PrivateKeyModel.DoesNotExist:
        response_data = {
            "code": "404",
            "message": "Data not found",
            "data": "NULL"
        }
        return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND)
    
    referralKey = request.data.get('referralKey', '')
    referredBy = request.data.get('referredBy', '')
    firstName_get = request.data.get('firstName', '')
    lastName_get = request.data.get('lastName', '')

    try:
        referralBy_obj = AppUsersModel.objects.get(email=referredBy, qrcode_status=True, isDeleted=0)
        referralBy_userid = referralBy_obj.email
    except:
        referralBy_userid = ''
        
    # Get the existing data from request.data
    existing_data = request.data.copy()

    # Add additional data to request.data
    additional_data = {
        'referred_by': referralBy_userid,
    }
    # Merge existing_data with additional_data
    merged_data = {**existing_data, **additional_data}

    # Check if the time difference is not more than 5 minutes
    current_time = timezone.now()
    time_difference = current_time - private_key_obj.createdAt
    max_time_difference = timedelta(minutes=5)

    if time_difference > max_time_difference:
        response_data = {
            "code": "404",
            "message": "Session expired",
            "data": "NULL"
        }
        return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND)
    
    # Check if the email already exists in the database
    email = request.data['email']
    try:
        existing_user = AppUsersModel.objects.get(email=email,isDeleted=0)
        response_data = {
            "code": "400",
            "message": "Email already exists",
            "data": "NULL"
        }
        return JsonResponse(response_data, status=status.HTTP_400_BAD_REQUEST)
    except AppUsersModel.DoesNotExist:
        pass  # Email does not exist, continue with user registration

    serializer = UserSerializer(data=merged_data)
    
    if serializer.is_valid():
        user_data = serializer.save()
        # Add the timestamp to the access token payload
        timestamp_in_milliseconds = int(datetime.now().timestamp() * 1000)
        userId = str(uuid.uuid4()) + str(timestamp_in_milliseconds)
        user_data.userId = userId
        user_data.save()
        # Generate a refresh token and access token
        refresh = RefreshToken.for_user(user_data)
        refresh.payload['userId'] = userId

        accessToken = AccessToken.for_user(user_data)
        serializer.instance.userId = userId
        serializer.instance.save()
        accessToken.payload['timestamp'] = timestamp_in_milliseconds
        accessToken.payload['email'] = user_data.email
        accessToken.payload['userId'] = userId

        # Save the tokens in the user model
        user_data.accessToken = str(accessToken)
        user_data.refreshToken = str(refresh)
        user_data.save()
        get_image_url = qrcode_generate(referralKey,firstName_get,lastName_get,user_data.email)
        if get_image_url:
            qrCodeImage_url = request.build_absolute_uri(get_image_url)
        else:
            default_qrcode = '/media/default_qrcode/teenzo_qr_code.jpeg'
            qrCodeImage_url = request.build_absolute_uri(default_qrcode)

        # Create AdsSubscriptions object for the registered user
        subscription_data = AdsSubscriptions.objects.create(userId=user_data.userId)
        subscription_data.save()    

        # Your FCM server key obtained from the Firebase Console
        server_key = 'AAAAZZ8iKqs:APA91bEf9ek9sGrBlV5kc-VJ-oSP4x_YVc0M-L-b-xoZSPsUL1FizwI7Tf1XtYPXYJTVWw786LqIYjB12lGOemJWDFnCpNxTbJm2PkAsFbmaUvlN8dsUT96A2D32xmRiivE-Dw4P_IWq'

        # Specify the topic name you want to subscribe the user to
        topic_name = 'SendNotification'

        # Subscribe the user to the specified topic
        # subscribe_user_to_topic(user_data.fcmToken, topic_name, server_key)

        # Customize your response structure
        response_data = {
            'code': '201',
            'message': 'Login successful',
            'data': {
                'isSubscribed' : 0,
                'userId' : user_data.userId,
                'firstName' : user_data.firstName,
                'lastName' : user_data.lastName,
                'userType': user_data.userType,
                'emailId' : user_data.email,
                'hashToken' : str(accessToken),
                'refreshToken' : str(refresh),
                'qrCodeImage':qrCodeImage_url,
                'referralKey':referralKey
            }
        }

        return JsonResponse(response_data, status=status.HTTP_201_CREATED)
    else:
        response_data = {
            "code": "400",
            "message": "Invalid data",
            "data": serializer.errors
        }
        return JsonResponse(response_data, status=status.HTTP_400_BAD_REQUEST)    
    
@swagger_auto_schema(method='post', operation_description="User for create new user", tags=['app-user'],
                     request_body=openapi.Schema(
                         type=openapi.TYPE_OBJECT,
                         properties={
                            'email': openapi.Schema(type=openapi.TYPE_STRING, description='Email'),
                            'privatekey': openapi.Schema(type=openapi.TYPE_STRING, description='Private Key'),
                            'returnkey': openapi.Schema(type=openapi.TYPE_STRING, description='Return Key'),
                            'fcmToken': openapi.Schema(type=openapi.TYPE_STRING, description='FCM token'),
                            'appVersion': openapi.Schema(type=openapi.TYPE_STRING, description='App version'),
                            'androidVersion': openapi.Schema(type=openapi.TYPE_STRING, description='Android version'),
                            'manufacturerName': openapi.Schema(type=openapi.TYPE_STRING, description='Manufacturer name'),
                            'modelName': openapi.Schema(type=openapi.TYPE_STRING, description='Model name'),
                         }
                     ))
@api_view(['POST'])
@authentication_classes([])  # Disable authentication for this specific view
@permission_classes([])  # Disable permission checks for this specific view
def check_user(request):
    token_key = request.headers.get('Authorization')
    
    if not token_key or token_key != "HelloWorld!":
        response_data = {
            "code": "401",
            "message": "Authorization header missing or incorrect",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    if 'returnkey' in request.data and request.data['returnkey']:
        response_data = {
            "code": "404",
            "message": "Data not found",
            "data": "NULL"
        }
        return JsonResponse(response_data,status=status.HTTP_404_NOT_FOUND)
    
    private_key_value = request.data['privatekey']
    # Check if private key exists in PrivateKeyModel
    try:
        private_key_obj = PrivateKeyModel.objects.get(key=private_key_value)
    except PrivateKeyModel.DoesNotExist:
        response_data = {
            "code": "404",
            "message": "Data not found",
            "data": "NULL"
        }
        return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND)

    # Check if the time difference is not more than 5 minutes
    current_time = timezone.now()
    time_difference = current_time - private_key_obj.createdAt
    max_time_difference = timedelta(minutes=5)

    if time_difference > max_time_difference:
        response_data = {
            "code": "404",
            "message": "Session expired",
            "data": "NULL"
        }
        return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND)
    
    try:
        expectedEmail = AppUsersModel.objects.get(email = request.data['email'],isDeleted=0)
        accessToken = expectedEmail.accessToken
        refreshToken = expectedEmail.refreshToken
        qrcodeStatus = expectedEmail.qrcode_status

        try:
            # Fetch the AppQrcodeModel instance
            qrCodeModel = AppQrcodeModel.objects.get(userId=str(expectedEmail.email))
            # Build the absolute URL for the qrCode image
            if qrcodeStatus:
                qrCodeImage_url = request.build_absolute_uri(qrCodeModel.qrCode.url)
                qrCodeReferralKey = qrCodeModel.qrCodeText
            else:
                default_qrcode = '/media/default_qrcode/teenzo_qr_code.jpeg'
                qrCodeImage_url = request.build_absolute_uri(default_qrcode)
                qrCodeReferralKey = ''
        except:
            default_qrcode = '/media/default_qrcode/teenzo_qr_code.jpeg'
            qrCodeImage_url = request.build_absolute_uri(default_qrcode)
            qrCodeReferralKey = ''
        
        result = app_user_db_utils.get_user_data_model(expectedEmail.email)
        result.fcmToken = request.data['fcmToken']
        result.appVersion = request.data['appVersion']
        result.androidVersion = request.data['androidVersion']
        result.manufacturerName = request.data['manufacturerName']
        result.modelName = request.data['modelName']
        result.save()

        # if notification_service.is_fcm_token_blacklisted(result.fcmToken ,expectedEmail.userId):
        #     message = f'FCM token is blacklisted for user {expectedEmail.userId}'
        #     LOGGER.info(json.dumps(message))
        # else:
        #     # Your FCM server key obtained from the Firebase Console
        #     server_key = 'AAAAZZ8iKqs:APA91bEf9ek9sGrBlV5kc-VJ-oSP4x_YVc0M-L-b-xoZSPsUL1FizwI7Tf1XtYPXYJTVWw786LqIYjB12lGOemJWDFnCpNxTbJm2PkAsFbmaUvlN8dsUT96A2D32xmRiivE-Dw4P_IWq'

        #     # Specify the topic name you want to subscribe the user to
        #     topic_name = 'SendNotification'

        #     if not is_user_subscribed_to_topic(result.fcmToken, topic_name):
        #         # Subscribe the user to the specified topic
        #         subscribe_user_to_topic(result.fcmToken , topic_name, server_key)
        #     else:
        #         message = f'User {expectedEmail.userId} is already subscribed to the topic {topic_name}'
        #         LOGGER.info(json.dumps(message))    

        response_data = {
            "code": "200",
            "message": "Data found",
            "data": {
                "isSubscribed" : result.isSubscribed,
                "userId" : result.userId,
                "firstName" : result.firstName,
                "lastName" : result.lastName,
                "userType": result.userType,
                "emailId" : result.email,
                "hashToken": accessToken,
                "refreshToken" : refreshToken,
                "qrCodeImage":qrCodeImage_url,
                "referralKey":qrCodeReferralKey,
            }
        }

        return JsonResponse(response_data)
    except AppUsersModel.DoesNotExist:
        response_data = {
                "code": "404",
                "message": "Data not found",
                "data": "NULL"
            }
        return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND)

    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    except jwt.ExpiredSignatureError:
        response_data = {
            "code": "401",
            "message": "Token expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    except jwt.PyJWTError:
        response_data = {
            "code": "401",
            "message": "Invalid token",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
@swagger_auto_schema(method='post', operation_description="User for create new user", tags=['app-user'],
                     request_body=openapi.Schema(
                         type=openapi.TYPE_OBJECT,
                         properties={
                            'userType': openapi.Schema(type=openapi.TYPE_INTEGER, description='User Type'),
                         }
                     ))
@api_view(['POST'])
@authentication_classes([])  # Disable authentication for this specific view
@permission_classes([])  # Disable permission checks for this specific view
def update_parent_child(request):
    token_key = request.headers.get('Authorization')   
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()

        accessToken = AccessToken(token)

        userId = accessToken.payload.get('userId')
        try:
            user_data = AppUsersModel.objects.get(userId=userId,isDeleted=0)
            user_data.userType = request.data['userType']
            user_data.save()
            response_data = {
                "code": "201",
                "message": "Details saved successfully",
                "data": "null"
            }

            return JsonResponse(response_data,status=status.HTTP_201_CREATED)
        except AppUsersModel.DoesNotExist:
            response_data = {
                "code": "404",
                "message": "User not found",
                "data": "null"
            }    
            return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND) 
    
    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
@swagger_auto_schema(method='get', operation_description="User for get parent detail", tags=['app-user'])
@api_view(['GET'])
@authentication_classes([])
@permission_classes([]) 
def parent_dashboard(request):
    token_key = request.headers.get('Authorization')
    
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()

        accessToken = AccessToken(token)
        userId = accessToken.payload.get('userId')
        activation_time = timezone.now()
        
        try:    
            user_data = AppUsersModel.objects.get(userId=str(userId),isDeleted=0)    
            if user_data.userType == 1:
                LOGGER.info("PARENT DASHBOARD:")
                child_data = ParentChildPairModel.objects.filter(parentId=userId,connectionStatus=1)
                log_data = {"Child Data": str(child_data)}
                LOGGER.info(json.dumps(log_data))
                user_data.activationTime = activation_time
                user_data.save()
                
                response_data = {
                    "code": "200",
                    "message": "Details found",
                    "data": {
                        "children": []
                    }
                }
                
                for child_entry in child_data:
                    child_user = AppUsersModel.objects.get(userId=child_entry.childId,isDeleted=0)
                    log_data = {"Child User": str(child_user)}
                    LOGGER.info(json.dumps(log_data))
                    app_data = AppListModel.objects.filter(userId=child_entry.childId,isDeleted=0).order_by('-id')
                    serializer = AppListDisplaySerializer(app_data, many=True, context={"request": request})

                    is_connected = ParentChildPairModel.objects.filter(parentId=userId, childId=child_entry.childId,connectionStatus=1).exists()
                    data = ParentChildPairModel.objects.get(parentId=userId, childId=child_entry.childId,isDeleted=0)
                    notification_data = NotificationModel.objects.filter(parentId=userId,childId=child_entry.childId,isDeleted=0)
                    last_location = ChildLatLongModel.objects.filter(childId=child_entry.childId,isDeleted=0).last()

                    child_info = {
                        "userId": child_user.userId,
                        "name": child_user.firstName,
                        "email": child_user.email,
                        "pairCode": child_entry.code
                    }

                    last_location_info = {
                        "lat": "",
                        "long": ""
                    }
                    
                    if last_location:
                        last_location_info['lat'] = last_location.lat
                        last_location_info['long'] = last_location.long
                    
                    connection_info = {
                        "lastNotificationReceived": "null",
                        "devicePairedTime": data.devicePairedTime,
                        "connectionStatus": 1 if is_connected else 0 ,
                        "disconnectedTime": data.disconnectedTime if data.disconnectedTime else "null"
                    }
                    
                    if notification_data.exists():
                        connection_info["lastNotificationReceived"] = notification_data.last().lastNotificationSent

                    device_info = {
                        "appVersion": child_user.appVersion,
                        "androidVersion": child_user.androidVersion,
                        "manufacturerName": child_user.manufacturerName,
                        "modelName": child_user.modelName
                    }

                    installed_app_list = serializer.data  # You can populate this with actual data
                    
                    child_data_entry = {
                        "childInfo": child_info,
                        "lastLocationInfo": last_location_info,
                        "connectionInfo": connection_info,
                        "deviceInfo": device_info,
                        "installedAppList": installed_app_list
                    }
                    
                    response_data["data"]["children"].append(child_data_entry)
                    topic_name = "SendNotification"
                    if is_user_subscribed_to_topic(user_data.fcmToken,topic_name):
                        log_data = f"The user is subscribed to the '{topic_name}' topic."
                        LOGGER.info(json.dumps(log_data))
                    else:
                        log_data = f"The user is not subscribed to the '{topic_name}' topic."
                        LOGGER.info(json.dumps(log_data))
                
                return JsonResponse(response_data)
            else:
                response_data = {
                "code": "404",
                "message": "Details not found",
                "data": "NULL"
            }
            return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND)
        except AppUsersModel.DoesNotExist:
            LOGGER.info(json.dumps("In Except block"))
            response_data = {
                "code": "404",
                "message": "User not found",
                "data": "NULL"
            }    
            return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND)
    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
@swagger_auto_schema(method='get', operation_description="User for get code", tags=['app-user'])
@api_view(['GET'])
@authentication_classes([])
@permission_classes([]) 
def pairing_code(request):
    token_key = request.headers.get('Authorization')
    
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)

    try:
        _, token = token_key.split()

        accessToken = AccessToken(token)
        userId = accessToken.payload.get('userId')
        try:
            user = AppUsersModel.objects.get(userId=userId,isDeleted=0)
            # Check if the user_type is 2 before generating the pairing code
            if user.userType != 2:
                response_data = {
                    "code": "403",
                    "message": "Permission Denied",
                    "data": "NULL"
                }
                return JsonResponse(response_data, status=status.HTTP_403_FORBIDDEN)
            
            key_length = 6
            pairingCode = ''.join(random.choices(string.ascii_letters + string.digits, k=key_length))

            try:
                data = PairingCodeModel.objects.get(userId=userId,isDeleted=0)
                data.code = pairingCode
                data.save()
            except PairingCodeModel.DoesNotExist:
                data = PairingCodeModel(code=pairingCode, userId=userId)
                data.save()

            response_data = {
                "code": "201",
                "message": "New Code Generated",
                "data": {
                    "paringCode": data.code
                }
            }

            return JsonResponse(response_data,status=status.HTTP_201_CREATED)
        except AppUsersModel.DoesNotExist:
            response_data = {
                "code": "404",
                "message": "User not found",
                "data": "NULL"
            }    
            return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND)
    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
@swagger_auto_schema(method='get', operation_description="User for get child detail", tags=['app-user'])
@api_view(['GET'])
@authentication_classes([])
@permission_classes([]) 
def child_dashboard(request):
    token_key = request.headers.get('Authorization')
    
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()

        accessToken = AccessToken(token)
        userId = accessToken.payload.get('userId')
        try:    
            user_data = AppUsersModel.objects.get(userId=userId,isDeleted=0)
            if user_data.userType == 2:
                try:
                    parent_data = ParentChildPairModel.objects.get(childId=userId,isDeleted=0)
                    parent_user = AppUsersModel.objects.get(userId=parent_data.parentId,isDeleted=0)
                    is_connected = ParentChildPairModel.objects.filter(parentId=parent_data.parentId, childId=userId,connectionStatus=1).exists()
                    data = ParentChildPairModel.objects.get(parentId=parent_data.parentId, childId=userId,isDeleted=0)
                    notification_data = NotificationModel.objects.filter(parentId=parent_data.parentId,childId=userId,isDeleted=0)
                    location_time = LatLongTimeUpdateModel.objects.first()
                    activation_time = timezone.now()
                    user_data.activationTime = activation_time
                    user_data.save()
                    
                    connection_info = {
                        "lastNotificationReceived": "null",  # Default value
                        "devicePairedTime": data.devicePairedTime,
                        "connectionStatus": 1 if is_connected else 0,
                        "disconnectedTime": data.disconnectedTime if data.disconnectedTime else "null"
                    }
                    if notification_data.exists():
                        connection_info["lastNotificationReceived"] = notification_data.last().lastNotificationSent

                    response_data = {
                            "code": "200",
                            "message": "Details Found",
                            "data": {
                                "locUploadTime": location_time.locUploadTime,
                                "userId":userId,
                                "parentInfo": {
                                "name": parent_user.firstName,
                                "email": parent_user.email,
                                "pairCode": parent_data.code
                                },
                                "connectionInfo": connection_info
                            }
                        }

                    return JsonResponse(response_data)    
                except ParentChildPairModel.DoesNotExist:
                    response_data = {
                        "code": "404",
                        "message": "Details not found",
                        "data": "NULL"
                    }
                    return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND) 
            else:
                response_data = {
                "code": "404",
                "message": "Details not found",
                "data": "NULL"
            }
            return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND) 
        except AppUsersModel.DoesNotExist:
            response_data = {
                "code": "404",
                "message": "User not found",
                "data": "NULL"
            }    
            return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND) 
    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
@swagger_auto_schema(method='post', operation_description="User for subscription", tags=['payment'],
                     request_body=openapi.Schema(
                         type=openapi.TYPE_OBJECT,
                         properties={
                            'month': openapi.Schema(type=openapi.TYPE_INTEGER, description='Month'),
                            'orderId': openapi.Schema(type=openapi.TYPE_STRING, description='Order ID'),
                            'productId': openapi.Schema(type=openapi.TYPE_STRING, description='PRODUCT ID'),
                            'purchaseToken': openapi.Schema(type=openapi.TYPE_STRING, description='Purchase Token'),
                            'isAcknowledged': openapi.Schema(type=openapi.TYPE_INTEGER, description='Acknowledged'),
                         }
                     ))
@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def payment(request):
    token_key = request.headers.get('Authorization')   
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()

        accessToken = AccessToken(token)

        userId = accessToken.payload.get('userId')
        try:
            user_data = AppUsersModel.objects.get(userId=userId,isDeleted=0)
            LOGGER.info(json.dumps("In Paymet API Function"))

            months = int(request.data['month'])

            # Calculate the expiration time based on the number of months
            expiration_time = timezone.now() + timedelta(days=months * 30)
            
            # Update user_data
            user_data.subscriptionTime = f"{months} Month"
            user_data.expireTime = expiration_time
            user_data.isSubscribed = 1
            user_data.orderId = request.data['orderId']
            user_data.productId = request.data['productId']
            user_data.purchaseToken = request.data['purchaseToken']

            log_data = {"Subscription orderId": request.data['orderId']}
            LOGGER.info(json.dumps(log_data))
            log_data = {"productId": request.data['productId']}
            LOGGER.info(json.dumps(log_data))
            log_data = {"userId": userId}
            LOGGER.info(json.dumps(log_data))
            
            user_data.isAcknowledged = request.data['isAcknowledged']
            user_data.save()
            
            response_data = {
                "code": "201",
                "message": "Details saved successfully",
                "data": "null"
            }

            return JsonResponse(response_data,status=status.HTTP_201_CREATED)
        except AppUsersModel.DoesNotExist:
            response_data = {
                "code": "404",
                "message": "User not found",
                "data": "null"
            }
            return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND) 
    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
@swagger_auto_schema(method='get', operation_description="User for get subscription detail", tags=['payment'])
@api_view(['GET'])
@authentication_classes([])
@permission_classes([]) 
def get_subscriptions(request):
    token_key = request.headers.get('Authorization')
    
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()

        accessToken = AccessToken(token) 
        Subscriptions_data = AvailableSubscriptions.objects.filter(isDeleted=0)
        Subscriptions_details = []
        for item in Subscriptions_data:
            detail = {
                "productName": item.productName,
                "productId": item.productId,
                "price": item.price,
                "duration":item.duration
            }
            Subscriptions_details.append(detail)

        response_data = {
            "code": "201",
            "message": "Success",
            "data": Subscriptions_details
        }

        return JsonResponse(response_data,status=status.HTTP_201_CREATED)    
        
    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
@swagger_auto_schema(method='get', operation_description="For register user into topic ", tags=['app-user'])
@api_view(['GET'])
@authentication_classes([])
@permission_classes([]) 
def send_all_user_fcm_tokens(request):
    # Fetch FCM tokens of all users
    user_data = AppUsersModel.objects.filter(isDeleted=0).values_list('fcmToken','userId')

    # Set the number of records per page
    records_per_page = 50

    # Create a paginator
    paginator = Paginator(user_data, records_per_page)

    # Get the requested page number from the query parameters
    page = request.GET.get('page', 1)

    try:
        # Get the page data
        page_data = paginator.page(page)
    except EmptyPage:
        # If the page is out of range, return an error response
        response_data = {
            "code": "404",
            "message": "Page not found",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND)

    # Iterate over each FCM token
    for fcm_token,user_id in page_data:
        if not fcm_token:
            # Skip processing if the FCM token is empty or blank
            message = f'FCM token is Empty for user {user_id}'
            LOGGER.info(json.dumps(message))
            continue

        if notification_service.is_fcm_token_blacklisted(fcm_token, user_id):
            message = f'FCM token is blacklisted for user {user_id}'
            LOGGER.info(json.dumps(message))
        else:
            # Your FCM server key obtained from the Firebase Console
            server_key = 'AAAAZZ8iKqs:APA91bEf9ek9sGrBlV5kc-VJ-oSP4x_YVc0M-L-b-xoZSPsUL1FizwI7Tf1XtYPXYJTVWw786LqIYjB12lGOemJWDFnCpNxTbJm2PkAsFbmaUvlN8dsUT96A2D32xmRiivE-Dw4P_IWq'

            # Specify the topic name you want to subscribe the user to
            topic_name = 'SendNotification'

            if not is_user_subscribed_to_topic(fcm_token, topic_name):
                # Subscribe the user to the specified topic
                subscribe_user_to_topic(fcm_token, topic_name, server_key)
            else:
                message = f'User {user_id} is already subscribed to the topic {topic_name}'
                LOGGER.info(json.dumps(message))

    # Continue processing the next batch by redirecting to the same API endpoint with an incremented page number
    next_page = int(page) + 1
    next_page_url = f'https://teenzo.com/app-user/api/register/fcm?page={next_page}'            

    # Return a response
    response_data = {
        "code": "200",
        "message": "FCM tokens processed successfully",
        "data": {
            "next_page_url": next_page_url
        }
    }
    return JsonResponse(response_data,status=status.HTTP_200_OK)    