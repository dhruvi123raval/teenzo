from app_users.models import AppUsersModel
from app_users.serializers import UserSerializer
import requests
import json
import logging

LOGGER = logging.getLogger('loki')

class AppUserDBUtils:

    def get_user_data_model(self, email):
        if AppUsersModel.objects.filter(email=email).exists():
            user_data = AppUsersModel.objects.get(email=email,isDeleted=0)
            return user_data
        
        return None
    
app_user_db_utils = AppUserDBUtils()

def subscribe_user_to_topic(fcm_token, topic_name, server_key):
    url = f'https://iid.googleapis.com/iid/v1/{fcm_token}/rel/topics/{topic_name}'
    headers = {
        'Authorization': f'key={server_key}',
        'Content-Type': 'application/json',
    }
    response = requests.post(url, headers=headers)

    if response.status_code == 200:
        msg = f"User subscribed to topic '{topic_name}' successfully."
        LOGGER.info(json.dumps(msg))
        print(f"User subscribed to topic '{topic_name}' successfully.")
        return True
    else:
        msg = f"Failed to subscribe user to topic '{topic_name}'."
        LOGGER.info(json.dumps(msg))
        print(f"Failed to subscribe user to topic '{topic_name}'.")
        return False

def is_user_subscribed_to_topic(fcm_token, topic_name):
    try:
        # Fetch the list of topics for the device using the Firebase Cloud Messaging REST API
        endpoint = f"https://iid.googleapis.com/iid/info/{fcm_token}?details=true"
        headers = {
            "Authorization": f"key=AAAAZZ8iKqs:APA91bEf9ek9sGrBlV5kc-VJ-oSP4x_YVc0M-L-b-xoZSPsUL1FizwI7Tf1XtYPXYJTVWw786LqIYjB12lGOemJWDFnCpNxTbJm2PkAsFbmaUvlN8dsUT96A2D32xmRiivE-Dw4P_IWq",
        }
        response = requests.get(endpoint, headers=headers)

        if response.status_code == 200:
            data = response.json()
            return topic_name in data.get("rel", {}).get("topics", [])
        else:
            log_data = f"Error checking subscription: {response.status_code}, {response.text}"
            LOGGER.info(json.dumps(log_data))
            return False
    except Exception as e:
        log_data = f"Error checking subscription: {e}"
        LOGGER.info(json.dumps(log_data))
        return False  