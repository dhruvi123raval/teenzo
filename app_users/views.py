import os
from django.conf import settings
from app_qrcode.models import AppQrcodeModel
from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic.edit import CreateView
from django.views.generic.edit import DeleteView
from django.views.generic.list import ListView
from django.views.generic.edit import UpdateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy,reverse
from app_users.models import AppUsersModel
from app_users.forms import AppUsersForm
from app_users.filters import UserNameFilter
from django_filters.views import FilterView
from django.http import JsonResponse

@method_decorator(login_required, name='dispatch')
class Create(SuccessMessageMixin, CreateView):
    model = AppUsersModel
    form_class = AppUsersForm
    template_name = 'app_users/create.html'
    success_url = reverse_lazy('app_users_list')
    success_message = "App User created successfully."

@method_decorator(login_required, name='dispatch')
class List(FilterView):
    model = AppUsersModel
    template_name = 'app_users/list.html'
    context_object_name = 'data'
    filterset_class = UserNameFilter
    paginate_by = 20

    def get_queryset(self):
        queryset = AppUsersModel.objects.filter(isDeleted=0).order_by('-id')
        for item in queryset:
            item_count = len(AppUsersModel.objects.filter(referred_by=item.email))
            item.referred_by_count = item_count
        return queryset
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        paginator = context['paginator']
        page = context['page_obj']

        continuous_count = (page.number - 1) * paginator.per_page
        context['continuous_count'] = continuous_count

        # Ensure that each item in data has referred_by_count property
        for item in context['data']:
            item_count = len(AppUsersModel.objects.filter(referred_by=item.email, isDeleted=0))
            setattr(item, 'referred_by_count', item_count)

        return context


@method_decorator(login_required, name='dispatch')
class Update(SuccessMessageMixin, UpdateView):
    model = AppUsersModel
    form_class = AppUsersForm
    template_name = 'app_users/update.html'
    success_url = reverse_lazy('app_users_list')
    success_message = "App User updated successfully."


@method_decorator(login_required, name='dispatch')
class Delete(SuccessMessageMixin,DeleteView):
    model = AppUsersModel
    template_name = "app_users/delete.html"
    success_url = reverse_lazy('app_users_list')
    success_message = "App User deleted successfully."

    def form_valid(self, form):
        # Get the instance to be deleted
        instance = self.get_object()

        # Delete the associated QR code image
        if instance.email:
            qrcode_instance=AppQrcodeModel.objects.filter(userId=instance.email).first()
            if qrcode_instance:
                # Assuming your image field is named 'qrcode_image'
                image_path = qrcode_instance.qrCode.path
                image_path = os.path.join(settings.MEDIA_ROOT, image_path)
                if os.path.exists(image_path):
                    os.remove(image_path)
                qrcode_instance.delete()

        # Call the parent class's delete method to complete the deletion
        return super().form_valid(form)


@method_decorator(login_required, name='dispatch')
class ReferralList(ListView):
    model = AppUsersModel
    template_name = 'app_users/referrallist.html'
    context_object_name = 'data'
    paginate_by = 20

    def get_queryset(self):
        pk_get = self.kwargs.get('pk')
        userid_get = AppUsersModel.objects.get(id=pk_get)
        queryset = AppUsersModel.objects.filter(referred_by=userid_get.email,isDeleted=0).order_by('-id')
        for item in queryset:
            item.referred_by_name = userid_get.firstName+" "+userid_get.lastName
        return queryset
    
def Autocomplete(request):
    if request.method == 'GET':
        qs = AppUsersModel.objects.filter(firstName__icontains=request.GET.get('term'))
        Names = list()
        for i in qs:
            Names.append(i.firstName)
        return JsonResponse(Names,safe=False)    

