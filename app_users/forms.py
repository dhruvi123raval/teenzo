from django import forms
from app_users.models import AppUsersModel


class AppUsersForm(forms.ModelForm):

    class Meta:
        model = AppUsersModel
        fields = ('firstName','lastName','email','qrcode_status')

    def __init__(self, *args, **kwargs):
        super(AppUsersForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'