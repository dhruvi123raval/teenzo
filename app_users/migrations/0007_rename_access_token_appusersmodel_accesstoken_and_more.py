# Generated by Django 4.0.5 on 2023-10-20 07:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_users', '0006_appusersmodel_access_token_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='appusersmodel',
            old_name='access_token',
            new_name='accessToken',
        ),
        migrations.RenameField(
            model_name='appusersmodel',
            old_name='android_version',
            new_name='androidVersion',
        ),
        migrations.RenameField(
            model_name='appusersmodel',
            old_name='app_version',
            new_name='appVersion',
        ),
        migrations.RenameField(
            model_name='appusersmodel',
            old_name='fcm_token',
            new_name='fcmToken',
        ),
        migrations.RenameField(
            model_name='appusersmodel',
            old_name='first_name',
            new_name='firstName',
        ),
        migrations.RenameField(
            model_name='appusersmodel',
            old_name='terms_and_condition',
            new_name='isTermsAndConditionAccepted',
        ),
        migrations.RenameField(
            model_name='appusersmodel',
            old_name='last_name',
            new_name='lastName',
        ),
        migrations.RenameField(
            model_name='appusersmodel',
            old_name='manufacturer_name',
            new_name='manufacturerName',
        ),
        migrations.RenameField(
            model_name='appusersmodel',
            old_name='model_name',
            new_name='modelName',
        ),
        migrations.RenameField(
            model_name='appusersmodel',
            old_name='refresh_token',
            new_name='refreshToken',
        ),
        migrations.RenameField(
            model_name='appusersmodel',
            old_name='user_id',
            new_name='userId',
        ),
        migrations.RenameField(
            model_name='appusersmodel',
            old_name='user_type',
            new_name='userType',
        ),
        migrations.RenameField(
            model_name='pairingcodemodel',
            old_name='created_at',
            new_name='createdAt',
        ),
        migrations.RenameField(
            model_name='pairingcodemodel',
            old_name='user_id',
            new_name='userId',
        ),
    ]
