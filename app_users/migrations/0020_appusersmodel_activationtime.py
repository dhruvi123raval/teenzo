# Generated by Django 4.0.5 on 2024-02-12 12:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_users', '0019_appusersmodel_notificationsent'),
    ]

    operations = [
        migrations.AddField(
            model_name='appusersmodel',
            name='activationTime',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
