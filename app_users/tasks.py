from __future__ import absolute_import, unicode_literals
from app_users.models import AppUsersModel
from child_location.models import ChildLatLongModel
from django.db.models import Max
from app_list.models import ParentChildPairModel
from celery import shared_task, Task
from datetime import datetime
from django.utils import timezone
from datetime import timedelta
from send_notification.NotificationService import notification_service
from django.core.exceptions import ObjectDoesNotExist
from celery.utils.log import get_task_logger
from celery.exceptions import SoftTimeLimitExceeded
from celery_once import QueueOnce
import redis

logger = get_task_logger(__name__)

@shared_task(name="subscription_expire")
def check_subscription():

    users_to_check = AppUsersModel.objects.exclude(expireTime=None)
    print("USER",users_to_check)

    current_datetime = datetime.now().date()
    print("CURRENT TIME",current_datetime)

    for user in users_to_check:
        # Check if expireTime is in the past or is today
        print("EXPIRE TIME",user.expireTime.date())
        if user.expireTime.date() <= current_datetime:
            # Subscription has expired, set isSubscribed to False
            user.isSubscribed = 0
            user.save()

    return "Done"

# Define a Redis connection for distributed lock
redis_conn = redis.StrictRedis(host='localhost', port=6379, db=0)

@shared_task(name="notify_parent", base=QueueOnce)
def check_latlong():
    # Acquire distributed lock
    lock = redis_conn.lock('notify_parent_lock', timeout=60)

    try:
        # Try to acquire the lock
        if lock.acquire(blocking=False):
            logger.info("Lock acquired. Running the task.")
            # Filter child users whose notificationSent is 0
            eligible_child_users = AppUsersModel.objects.filter(userType=2, notificationSent=0, isDeleted=0)
            print("User with Notification flag 0",eligible_child_users)

            # Current time
            current_time = timezone.now()

            for child_user in eligible_child_users:
                try:
                    # Retrieve the latest entry for the child
                    latest_entry = ChildLatLongModel.objects.filter(childId=child_user.userId).latest('createdAt')
                    print("Latest Entries",latest_entry)

                    # Calculate time difference
                    time_difference = current_time - latest_entry.createdAt

                    # Check if the time difference is greater than 60 minutes
                    if time_difference > timedelta(minutes=60):
                        try:
                            parent_data = ParentChildPairModel.objects.get(childId=child_user.userId, isDeleted=0, connectionStatus=1)
                            print("Child ID", child_user.userId)
                            print("Parent ID", parent_data.parentId)

                            # Send notification to parent
                            # message = f'{child_user.firstName} has been disconnected'
                            message ='Check your child device connected or not?'
                            if child_user.notificationSent == 0:
                                print(f"Notification send in parent {parent_data.parentId}")
                                result = notification_service.notification_user(parent_data.parentId,message)

                            if result and result.get("success", 0) == 1:
                                print("Notification Sent successfully")
                                # if the notification was sent successfully Update notificationSent to 1 for the child user
                                child_user.notificationSent = 1
                                child_user.save()

                        except ParentChildPairModel.DoesNotExist:
                            # Handle the case where the query does not find a matching object
                            print(f"No matching ParentChildPairModel for Child ID {child_user.userId}")
                except ObjectDoesNotExist as e:
                    # Handle the case where the query does not find a matching object
                    print(f"No matching ChildLatLongModel for Child ID {child_user.userId}: {e}")
            # Update notificationSent to 0 for all child users
            user_data = AppUsersModel.objects.filter(userType=2, isDeleted=0)
            for i in user_data:
                print("Set all child user to 0")
                i.notificationSent = 0
                i.save()            

            return "Done"
        
        else:
            logger.warning("Failed to acquire lock. Another instance is running.")
            return "Failed to acquire lock. Another instance is running."
        
    except SoftTimeLimitExceeded:
        logger.warning("Task exceeded time limit.")

    finally:
        if lock.acquire(blocking=False):
        # Release the lock in a finally block to ensure it gets released
            lock.release()

    return "Done"        