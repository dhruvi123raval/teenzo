from app_users.models import AppUsersModel
from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppUsersModel
        fields = ['id', 'email','firstName', 'lastName','fcmToken','appVersion','androidVersion','manufacturerName','modelName','userType','userId','isTermsAndConditionAccepted','accessToken','refreshToken','referred_by']

class CreateUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppUsersModel
        fields = ['id', 'mobile_no', 'full_name', 'email', 'country_name']        