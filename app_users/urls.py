from django.urls import path
from app_users import views
from app_users.apis import api_views

urlpatterns = [
    path('create', views.Create.as_view(), name="app_users_create"),
    path('list/', views.List.as_view(), name="app_users_list"),
    path('update/<int:pk>', views.Update.as_view(), name="app_users_update"),
    path('delete/<int:pk>', views.Delete.as_view(), name="app_users_delete"),
    # path('api/create/', api_views.UserListCreateView.as_view()),
    path('new-access-token/', api_views.new_access_token, name='access_token'),
    path('new-refresh-token/', api_views.new_refresh_token, name='new_refresh_token'),
    path('api/signup/', api_views.app_user_register),
    path('api/login/', api_views.check_user),
    path('api/parent-child/', api_views.update_parent_child),
    path('api/dashboard/parent', api_views.parent_dashboard),
    path('api/dashboard/child', api_views.child_dashboard),
    path('api/generate-code', api_views.pairing_code),
    path('api/payment', api_views.payment),
    path('api/subscriptions/get', api_views.get_subscriptions),
    path('api/register/fcm', api_views.send_all_user_fcm_tokens),
    path('referrallist/<int:pk>', views.ReferralList.as_view(), name="app_referral_users_list"),
    # path('api/view/<int:pk>/', api_views.UserRetrieveView.as_view(), name='app-users-detail'),
    path('app-user-autocomplete/', views.Autocomplete, name='app_user_autocomplete'),
]