from django.db import models
from app_list.models import ParentChildPairModel
from django.db.models import Q

class AppUsersModel(models.Model):
    user_types = (
        (1, 'Parent'),
        (2, 'Child')
    )
    deleteStatus = (
        (0, 'Not deleted'),
        (1, 'deleted')
    )
    subscriptionStatus = (
        (0, 'Not subscribed'),
        (1, 'subscribed')
    )
    notificationStatus = (
        (0, 'Not sent'),
        (1, 'sent')
    )
    acknowledgedStatus = (
        (0, 'Not acknowledged'),
        (1, 'acknowledged')
    )
    firstName = models.CharField(max_length=100, verbose_name="First Name")
    lastName = models.CharField(max_length=100, verbose_name="Last Name")
    email = models.CharField(max_length=100, verbose_name="Email",default='')
    userType = models.SmallIntegerField(choices=user_types,default=0)
    userId = models.CharField(max_length=100, verbose_name="User ID",default='')
    isTermsAndConditionAccepted = models.BooleanField(default=True)
    createdAt = models.DateTimeField(auto_now_add=True) 
    updatedAt = models.DateTimeField(auto_now=True)
    appVersion = models.CharField(max_length=100, verbose_name="App Version",default='')
    androidVersion = models.CharField(max_length=100, verbose_name="Android Version",default='')
    manufacturerName = models.CharField(max_length=100, verbose_name="Manufacturer Name",default='')
    modelName = models.CharField(max_length=100, verbose_name="Model Name",default='')
    fcmToken = models.CharField(max_length=200, verbose_name="Fcm ID", blank=True, default='')
    accessToken = models.CharField(max_length=500, blank=True, default='')
    refreshToken = models.CharField(max_length=500, blank=True, default='')
    isDeleted = models.SmallIntegerField(choices=deleteStatus,default=0)
    isSubscribed = models.SmallIntegerField(choices=subscriptionStatus,default=0)
    activationTime = models.DateTimeField(null=True, blank=True)
    subscriptionTime = models.CharField(max_length=100,default='')
    expireTime = models.DateTimeField(null=True, blank=True)
    orderId = models.CharField(max_length=500,default='')
    productId = models.CharField(max_length=500,default='')
    purchaseToken = models.CharField(max_length=500,default='')
    billingResponseCode = models.CharField(max_length=500,default='',null=True)
    billingResult = models.JSONField(null=True)
    isAcknowledged = models.SmallIntegerField(choices=acknowledgedStatus,default=0)
    referred_by = models.CharField(max_length=150, verbose_name="Referred By",default='',blank=True)
    qrcode_status = models.BooleanField(default=False)
    notificationSent = models.SmallIntegerField(choices=notificationStatus,default=0)

    def delete(self, *args, **kwargs):
        # Update associated ParentChildPairModel instances
        parent_pairs = ParentChildPairModel.objects.filter(Q(parentId=self.userId) | Q(childId=self.userId))
        for parent_pair in parent_pairs:
            parent_pair.connectionStatus = 0
            parent_pair.isDeleted = 1
            parent_pair.save()

        # Set isDeleted flag to 1
        self.isDeleted = 1
        self.save()

class PairingCodeModel(models.Model):
    deleteStatus = (
        (0, 'Not deleted'),
        (1, 'deleted')
    )
    code = models.CharField(max_length=300, verbose_name="code")
    createdAt = models.DateTimeField(auto_now_add=True)    
    userId = models.CharField(max_length=100, verbose_name="User ID",default='')
    isDeleted = models.SmallIntegerField(choices=deleteStatus,default=0)

    def delete(self):
        self.isDeleted = 1
        self.save()

# class ChildQRcodeModel(models.Model):
#     userId = models.CharField(max_length=100, verbose_name="User ID", default='')
#     qrCode = models.ImageField(upload_to='child_qrcode/',verbose_name="qrcode")
#     created_at = models.DateTimeField(auto_now_add=True)
#     updated_at = models.DateTimeField(auto_now=True)        
