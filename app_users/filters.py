import django_filters
from django import forms
from django.db.models import Q
from app_users.models import AppUsersModel


class UserNameFilter(django_filters.FilterSet):
    search  = django_filters.CharFilter(
        method='filter_by_all_fields',
        label="Search",
        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter first name or email'})
    )

    class Meta:
        model = AppUsersModel
        fields = []

    def filter_by_all_fields(self, queryset, name, value):
        return queryset.filter(
            Q(firstName__icontains=value) | Q(email__icontains=value)
        )

    def __init__(self, *args, **kwargs):
        super(UserNameFilter, self).__init__(*args, **kwargs)
        self.filters['search'].label = "Search"
        self.filters['search'].field.widget.attrs.update({'class': 'form-control'})