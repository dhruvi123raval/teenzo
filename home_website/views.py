from django.shortcuts import render,redirect
from django.urls import reverse
from home_page.models import HomePageModel,How_it_works,IconModel
from features.models import FeatureModel,InnerFeatureModel,PricingModel
from testimonials.models import Testimonials,PeopleModel,AboutModel
from screenshot.models import ScreenshotModel,ImageModel
from faqs.models import FaQsModel,Que_AnsModel
from contact.models import ContactModel,FillupModel,Terms_and_conditionModel,PrivacyModel,RefundModel
from offer.models import OfferModel
from app_users.models import AppUsersModel

def home_website(request):
    data = dict()
    data['home'] = HomePageModel.objects.first()
    data['how_works'] = How_it_works.objects.first()
    data['icons'] = IconModel.objects.all()
    data['features'] = FeatureModel.objects.first()
    data['inner_feature'] = InnerFeatureModel.objects.all()
    data['pricing'] = PricingModel.objects.first()
    data['testimonial'] = Testimonials.objects.first()
    data['people'] = PeopleModel.objects.all()
    data['screenshot'] = ScreenshotModel.objects.first()
    data['images'] = ImageModel.objects.all()
    data['faqs'] = FaQsModel.objects.first()
    data['QueAns'] = Que_AnsModel.objects.all()
    data['contact'] = ContactModel.objects.first()
    data['about'] = AboutModel.objects.first()
    data['users'] = AppUsersModel.objects.filter(isDeleted=0).count()

    if request.method == 'POST':
        email = request.POST.get('email')
        message = request.POST.get('message')
        
        if email and message:
            user = FillupModel(email=email, message=message)
            user.save()
            return redirect('{}?message={}'.format(reverse('home_website'), 'Thank you for contacting us'))
        
        else:
            error = 'Please fill in all fields'
            return render(request, 'home_website/index.html', {'error': error})

    else:
        return render(request, 'home_website.html',data)
    
def offer(request):
    data = dict()
    data['offers'] = OfferModel.objects.first()

    return render(request, 'offer_page.html', data)

def terms_condition(request):
    data = dict()
    data['terms_condition'] = Terms_and_conditionModel.objects.first()
    return render(request, 'terms_condition.html',data)

def privacy(request):
    data = dict()
    data['privacy'] = PrivacyModel.objects.first()
    return render(request, 'privacy.html',data)

def refund(request):
    data = dict()
    data['refund'] = RefundModel.objects.first()
    return render(request, 'refund.html',data)


