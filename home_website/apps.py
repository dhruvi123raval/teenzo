from django.apps import AppConfig


class HomeWebsiteConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'home_website'
