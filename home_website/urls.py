from django.urls import path
from home_website import views

urlpatterns = [
    path('', views.home_website, name='home_website'),
    path('offer-policy', views.offer, name='offer_policy'),
    path('terms-condition', views.terms_condition, name='terms_condition'),
    path('privacy-policy', views.privacy, name='privacy'),
    path('refund-policy', views.refund, name='refund'),
]