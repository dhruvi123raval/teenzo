from django.urls import path
from custom_auth import views

urlpatterns = [
    path('login', views.login_page, name='adminside_login'),
    path('dashboard', views.admin_dashboard, name='admin_dashboard'),
    path('logout', views.auth_logout, name="auth_logout"),
    # path('reset_password/', views.reset_password, name='reset_password'),
]