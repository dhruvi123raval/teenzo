import re

from django import template
from django.urls import reverse, NoReverseMatch

register = template.Library()


@register.simple_tag(takes_context=True)
def active(context, pattern_or_urlname, n_id = None):
    try:

        if n_id:
            pattern = '^' + reverse(pattern_or_urlname, args=[n_id])
        else:
            pattern = '^' + reverse(pattern_or_urlname)

    except NoReverseMatch:
        pattern = pattern_or_urlname

    path = context['request'].path

    if re.search(pattern, path):
        return 'active'

    return ''

@register.simple_tag
def field_in_list(field_name, field_list):
    return field_name in field_list