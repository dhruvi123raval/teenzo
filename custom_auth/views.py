import datetime
from django.contrib import auth
from django.contrib import messages
from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.contrib.auth.models import User
from app_users.models import AppUsersModel
from walkthrough.models import WalkthroughModel,DisclaimerModel,FirstScreenModel
import logging

logger = logging.getLogger(__name__)

def login_page(request):
    if request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password, is_superuser=True)
        if user:
            auth.login(request, user)
            messages.success(request, 'Welcome in Dashboard')
            return redirect(reverse('admin_dashboard'))
        else:
            messages.error(request, 'Please enter valid username and password.')
            return redirect(reverse('adminside_login'))

    return render(request, 'login.html')


@login_required
def admin_dashboard(request):
    logging.basicConfig(level=logging.INFO)
    logger.info("Test log entry")
    data = dict()
    data['app_user'] = AppUsersModel.objects.filter(isDeleted=0).count()
    data['app_walkthrough'] = WalkthroughModel.objects.filter(isDeleted=0).count()
    data['app_screen'] = FirstScreenModel.objects.filter(isDeleted=0).count()
    data['app_disclaimer'] = DisclaimerModel.objects.filter(isDeleted=0).count()
    
    return render(request, 'index.html', data)


def auth_logout(request):
    auth.logout(request)
    messages.success(request, 'Thanks for Visiting.')
    return redirect(reverse('adminside_login'))


