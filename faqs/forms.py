from django import forms
from faqs.models import FaQsModel,Que_AnsModel

class FaQsForm(forms.ModelForm):
    class Meta:
        model = FaQsModel
        fields = ('img','title','description')

    def __init__(self, *args, **kwargs):
        super(FaQsForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control' 

class Que_AnsForm(forms.ModelForm):
    class Meta:
        model = Que_AnsModel
        fields = ('question','answer')

    def __init__(self, *args, **kwargs):
        super(Que_AnsForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'             