from faqs import views
from django.urls import path

urlpatterns = [
    path('update/<int:pk>', views.FaQsUpdate.as_view(), name="FaQs_update"),
    path('add/', views.FaQsCreate.as_view(), name="FaQs_create"),
    path('data/add', views.Create.as_view(), name='data_add'),
    path('data/update/<int:pk>', views.Update.as_view(), name='data_update'),
    path('data/list', views.List.as_view(), name="que_ans_list"),
    path('data/delete/<int:pk>', views.Delete.as_view(), name="data_delete"),
]    