from django.db import models

# Create your models here.
class FaQsModel(models.Model):
    img = models.ImageField(help_text = "Image size: 254px * 506px",verbose_name="Image",null=True)
    title = models.CharField(max_length=200)
    description = models.TextField()

class Que_AnsModel(models.Model):
    question = models.CharField(max_length=200)
    answer = models.TextField()    

