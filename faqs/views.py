from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic.edit import UpdateView ,CreateView,DeleteView
from django.views.generic.list import ListView
from faqs.models import FaQsModel,Que_AnsModel
from django.urls import reverse_lazy
from faqs.forms import FaQsForm,Que_AnsForm


# Create your views here.
@method_decorator(login_required, name='dispatch')
class FaQsUpdate(SuccessMessageMixin, UpdateView):
	model = FaQsModel
	form_class = FaQsForm
	template_name = 'faqs/update.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Details updated successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("FaQs_update", kwargs={"pk": pk})
	
@method_decorator(login_required, name='dispatch')
class FaQsCreate(SuccessMessageMixin, CreateView):
	form_class = FaQsForm
	template_name = 'faqs/create.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Details created successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("FaQs_update", kwargs={"pk": pk})

@method_decorator(login_required,name='dispatch')
class Create(SuccessMessageMixin, CreateView):
    form_class = Que_AnsForm
    template_name = 'faqs/data_create.html'
    success_url = reverse_lazy('que_ans_list')
    success_message = "Data added successfully."

@method_decorator(login_required,name='dispatch')
class Update(SuccessMessageMixin, UpdateView):
    model = Que_AnsModel
    form_class = Que_AnsForm
    template_name = 'faqs/data_update.html'
    success_url = reverse_lazy('que_ans_list')
    success_message = "Data updated successfully."    

@method_decorator(login_required,name='dispatch')
class List(ListView):
    model = Que_AnsModel
    template_name = 'faqs/data_list.html'
    context_object_name = 'data'
    
@method_decorator(login_required,name='dispatch')
class Delete(SuccessMessageMixin,DeleteView):
    model = Que_AnsModel
    success_url = reverse_lazy('que_ans_list')
    template_name = "faqs/data_delete.html"
    success_message = "Data deleted successfully."    	