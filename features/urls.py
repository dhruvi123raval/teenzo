from features import views
from django.urls import path

urlpatterns = [
    path('update/<int:pk>', views.FeatureUpdate.as_view(), name="feature_update"),
    path('add/', views.FeatureCreate.as_view(), name="feature_create"),
    path('inner/add', views.Create.as_view(), name='feature_inner_add'),
    path('inner/update/<int:pk>', views.Update.as_view(), name='feature_inner_update'),
    path('inner/list', views.List.as_view(), name="feature_inner_list"),
    path('inner/delete/<int:pk>', views.Delete.as_view(), name="feature_inner_delete"),
    path('price/update/<int:pk>', views.PricingUpdate.as_view(), name="price_update"),
    path('price/add/', views.PricingCreate.as_view(), name="price_create"),

]    