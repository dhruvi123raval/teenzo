from django.db import models

# Create your models here.
class FeatureModel(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    img = models.ImageField(help_text = "Image size: 381px * 416px",verbose_name="Image")

class InnerFeatureModel(models.Model):
    inner_title = models.CharField(max_length=200)    
    inner_description = models.TextField()

class PricingModel(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()