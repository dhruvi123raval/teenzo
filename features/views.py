from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic.edit import UpdateView ,CreateView,DeleteView
from django.views.generic.list import ListView
from features.models import FeatureModel,InnerFeatureModel,PricingModel
from django.urls import reverse_lazy
from features.forms import FeatureForm,InnerFeatureForm,PricingForm


# Create your views here.
@method_decorator(login_required, name='dispatch')
class FeatureUpdate(SuccessMessageMixin, UpdateView):
	model = FeatureModel
	form_class = FeatureForm
	template_name = 'features/update.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Features updated successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("feature_update", kwargs={"pk": pk})
	
@method_decorator(login_required, name='dispatch')
class FeatureCreate(SuccessMessageMixin, CreateView):
	form_class = FeatureForm
	template_name = 'features/create.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Features created successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("feature_update", kwargs={"pk": pk})	

@method_decorator(login_required,name='dispatch')
class Create(SuccessMessageMixin, CreateView):
    form_class = InnerFeatureForm
    template_name = 'features/inner_create.html'
    success_url = reverse_lazy('feature_inner_list')
    success_message = "Feature added successfully."

@method_decorator(login_required,name='dispatch')
class Update(SuccessMessageMixin, UpdateView):
    model = InnerFeatureModel
    form_class = InnerFeatureForm
    template_name = 'features/inner_update.html'
    success_url = reverse_lazy('feature_inner_list')
    success_message = "Feature updated successfully."    

@method_decorator(login_required,name='dispatch')
class List(ListView):
    model = InnerFeatureModel
    template_name = 'features/inner_list.html'
    context_object_name = 'data'
    
@method_decorator(login_required,name='dispatch')
class Delete(SuccessMessageMixin,DeleteView):
    model = InnerFeatureModel
    success_url = reverse_lazy('feature_inner_list')
    template_name = "features/inner_delete.html"
    success_message = "Feature deleted successfully."    

@method_decorator(login_required, name='dispatch')
class PricingUpdate(SuccessMessageMixin, UpdateView):
	model = PricingModel
	form_class = PricingForm
	template_name = 'features/price_update.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Price updated successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("price_update", kwargs={"pk": pk})
	
@method_decorator(login_required, name='dispatch')
class PricingCreate(SuccessMessageMixin, CreateView):
	form_class = PricingForm
	template_name = 'features/price_create.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Price created successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("price_update", kwargs={"pk": pk})	