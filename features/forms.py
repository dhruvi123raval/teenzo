from django import forms
from features.models import FeatureModel,InnerFeatureModel,PricingModel

class FeatureForm(forms.ModelForm):
    class Meta:
        model = FeatureModel
        fields = ('title','description','img')

    def __init__(self, *args, **kwargs):
        super(FeatureForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

class InnerFeatureForm(forms.ModelForm):
    class Meta:
        model = InnerFeatureModel
        fields = ('inner_title','inner_description')

    def __init__(self, *args, **kwargs):
        super(InnerFeatureForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'  

class PricingForm(forms.ModelForm):
    class Meta:
        model = PricingModel
        fields = ('title','description')

    def __init__(self, *args, **kwargs):
        super(PricingForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'                      