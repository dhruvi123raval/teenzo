# Generated by Django 4.0.5 on 2023-09-28 10:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('screenshot', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ImageModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('img', models.ImageField(help_text='Image size: 195px * 424px', upload_to='', verbose_name='Image')),
                ('title', models.CharField(max_length=200)),
                ('description', models.TextField()),
            ],
        ),
    ]
