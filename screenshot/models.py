from django.db import models

# Create your models here.
class ScreenshotModel(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()

class ImageModel(models.Model):
    img = models.ImageField(help_text = "Image size: 195px * 424px",verbose_name="Image")
    title = models.CharField(max_length=200)
    description = models.TextField()    