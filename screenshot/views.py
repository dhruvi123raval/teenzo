from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic.edit import UpdateView ,CreateView,DeleteView
from django.views.generic.list import ListView
from screenshot.models import ScreenshotModel,ImageModel
from django.urls import reverse_lazy
from screenshot.forms import ScreenshotForm,ImageForm


# Create your views here.
@method_decorator(login_required, name='dispatch')
class ScreenshotUpdate(SuccessMessageMixin, UpdateView):
	model = ScreenshotModel
	form_class = ScreenshotForm
	template_name = 'update.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Details updated successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("screenshot_update", kwargs={"pk": pk})
	
@method_decorator(login_required, name='dispatch')
class ScreenshotCreate(SuccessMessageMixin, CreateView):
	form_class = ScreenshotForm
	template_name = 'create.html' 
	success_url = reverse_lazy('admin_dashboard')
	success_message = "Details created successfully."	   

	def get_success_url(self):
		pk = self.kwargs["pk"]
		return reverse_lazy("screenshot_update", kwargs={"pk": pk})

@method_decorator(login_required,name='dispatch')
class Create(SuccessMessageMixin, CreateView):
    form_class = ImageForm
    template_name = 'image_create.html'
    success_url = reverse_lazy('image_list')
    success_message = "Image added successfully."

@method_decorator(login_required,name='dispatch')
class Update(SuccessMessageMixin, UpdateView):
    model = ImageModel
    form_class = ImageForm
    template_name = 'image_update.html'
    success_url = reverse_lazy('image_list')
    success_message = "Image updated successfully."    

@method_decorator(login_required,name='dispatch')
class List(ListView):
    model = ImageModel
    template_name = 'image_list.html'
    context_object_name = 'data'
    
@method_decorator(login_required,name='dispatch')
class Delete(SuccessMessageMixin,DeleteView):
    model = ImageModel
    success_url = reverse_lazy('image_list')
    template_name = "image_delete.html"
    success_message = "Image deleted successfully."    	