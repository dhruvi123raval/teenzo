from django import forms
from screenshot.models import ScreenshotModel,ImageModel

class ScreenshotForm(forms.ModelForm):
    class Meta:
        model = ScreenshotModel
        fields = ('title','description')

    def __init__(self, *args, **kwargs):
        super(ScreenshotForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'  

class ImageForm(forms.ModelForm):
    class Meta:
        model = ImageModel
        fields = ('img','title','description')

    def __init__(self, *args, **kwargs):
        super(ImageForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'              