from screenshot import views
from django.urls import path

urlpatterns = [
    path('update/<int:pk>', views.ScreenshotUpdate.as_view(), name="screenshot_update"),
    path('add/', views.ScreenshotCreate.as_view(), name="screenshot_create"),
    path('image/add', views.Create.as_view(), name='image_add'),
    path('image/update/<int:pk>', views.Update.as_view(), name='image_update'),
    path('image/list', views.List.as_view(), name="image_list"),
    path('image/delete/<int:pk>', views.Delete.as_view(), name="image_delete"),
]    