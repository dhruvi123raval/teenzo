from django.urls import path
from app_list import views
from app_list.apis import api_views

urlpatterns = [
    path('api/update/', api_views.update_app_list),
    path('api/get/app-list', api_views.get_app_list),
    path('api/get/application-list', api_views.get_application_list),
    path('api/select/', api_views.select_app_list),
    path('api/get/selected-app', api_views.get_select_app_list),
    path('api/get/selection-list', api_views.get_selection_app_list),
    path('api/pair',api_views.pairing),
    path('api/unpair',api_views.unpair)
]