from app_list.models import AppListModel,AppMasterModel
from app_users.models import AppUsersModel
from rest_framework import status
from rest_framework.response import Response
import logging
import json

# logger = logging.getLogger('django')
LOGGER = logging.getLogger('loki') 

def add_list_data(userId, app_entries,userName):
    LOGGER.info(json.dumps("inside add list data")) 
    app_list_ids = []  # Create a list to store the IDs of the created AppListModel instances
    app_already_installed = False

    for app_entry in app_entries:
        app_id = app_entry['id']  # Assuming 'id' is the field for the app's ID in AppMasterModel

        # Fetch the AppMasterModel instance using app_id
        try:
            app_instance = AppMasterModel.objects.get(id=app_id)
            log_data = {"App instance": app_instance.id}
            LOGGER.info(json.dumps(log_data)) 
        except AppMasterModel.DoesNotExist:
            print(f"AppMasterModel with id {app_id} does not exist")
            continue
        
        # Count existing entries that match the specified conditions
        existing_entries_count = AppListModel.objects.filter(app=app_instance, userId=userId, userName=userName).count()

        if existing_entries_count == 0:
            # No existing entry, create a new one
            user_app_list, created = AppListModel.objects.get_or_create(app=app_instance, userId=userId, userName=userName)

            if created:
                log_data = {"New app": user_app_list.id}
                LOGGER.info(json.dumps(log_data))
                app_list_ids.append(user_app_list.id)
            else:
                log_data = {"Already installed": user_app_list.id}
                LOGGER.info(json.dumps(log_data))
                app_already_installed = True
        else:
            # Existing entry found, handle accordingly (e.g., log, skip, or return an error)
            log_data = {"Already installed": "Entry already exists"}
            LOGGER.info(json.dumps(log_data))
            app_already_installed = True    

    if app_already_installed:
        response_data = {
            "code": "400",
            "message": "One or more apps are already installed",
            "data": "null"
        } 
        return Response(response_data, status=status.HTTP_400_BAD_REQUEST)
    else:
        return app_list_ids