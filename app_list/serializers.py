from app_list.models import AppMasterModel,AppListModel
from rest_framework import serializers

class AppMasterSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppMasterModel
        fields = ['id','appName', 'packageName', 'appLogo','isPremium']
 
class AppListDisplaySerializer(serializers.ModelSerializer):
    app = AppMasterSerializer()

    class Meta:
        model = AppListModel
        fields = ['id', 'userName','app']
