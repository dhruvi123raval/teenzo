from django.db import models

# Create your models here.
class AppMasterModel(models.Model):
    isAppStatus = (
        (0, 'Non Premium'),
        (1, 'Premium'),
    )
    deleteStatus = (
        (0, 'Not deleted'),
        (1, 'deleted')
    )
    appName = models.CharField(max_length=200)
    packageName = models.CharField(max_length=200)
    appLogo = models.CharField(max_length=200)
    isPremium = models.SmallIntegerField(choices=isAppStatus, default=0)
    isDeleted = models.SmallIntegerField(choices=deleteStatus,default=0)

    def delete(self):
        self.isDeleted = 1
        self.save()

class AppListModel(models.Model): 
    deleteStatus = (
        (0, 'Not deleted'),
        (1, 'deleted')
    )
    isAppSelected = (
        (1, 'Not selected'),
        (0, 'Selected'),
    )
    isDeleted = models.SmallIntegerField(choices=deleteStatus,default=0)
    userId = models.CharField(max_length=100,default='')
    userName = models.CharField(max_length=100,default='') 
    app = models.ForeignKey(AppMasterModel, on_delete=models.CASCADE, related_name="user_app_list",default='')
    isSelected = models.SmallIntegerField(choices=isAppSelected,default=1)

    def delete(self):
        self.isDeleted = 1
        self.save()

class ParentChildPairModel(models.Model):
    connection = (
        (0, 'Disconnected'),
        (1, 'Connected')
    )
    deleteStatus = (
        (0, 'Not deleted'),
        (1, 'deleted')
    )
    parentId = models.CharField(max_length=100,default='')
    childId = models.CharField(max_length=100,default='')
    code = models.CharField(max_length=100,default='') 
    connectionStatus = models.SmallIntegerField(choices=connection,default=0)
    isDeleted = models.SmallIntegerField(choices=deleteStatus,default=0)
    devicePairedTime = models.DateTimeField(auto_now_add=True)
    disconnectedTime = models.DateTimeField(null=True, blank=True)

    def delete(self):
        self.isDeleted = 1
        self.save()


