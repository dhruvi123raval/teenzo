import datetime
from app_list.serializers import AppMasterSerializer,AppListDisplaySerializer
from app_list.models import AppMasterModel,AppListModel,ParentChildPairModel
from app_users.models import AppUsersModel,PairingCodeModel
from send_notification.models import NotificationModel
from app_list.app_list_db_utils import add_list_data
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import api_view,authentication_classes,permission_classes
from rest_framework.response import Response
from rest_framework import status
from django.http import JsonResponse,HttpResponse,Http404
import jwt
from rest_framework_simplejwt.exceptions import TokenError
from rest_framework_simplejwt.tokens import RefreshToken, AccessToken
from django.utils import timezone
from send_notification.NotificationService import notification_service
import logging
import json
from django.db import transaction
from rest_framework.exceptions import APIException

# logger = logging.getLogger(__name__)
LOGGER = logging.getLogger('loki')

@swagger_auto_schema(method='post', operation_description="for create app list", tags=['app-list'],
                     request_body=openapi.Schema(
                        type=openapi.TYPE_OBJECT,
                        properties={
                            'installedAppList': openapi.Schema(
                                type=openapi.TYPE_ARRAY,
                                items=openapi.Schema(type=openapi.TYPE_OBJECT),
                            ),
                        }
                    )
                    )
@api_view(['POST'])
@authentication_classes([])
@permission_classes([]) 
@transaction.atomic
def update_app_list(request):
    token_key = request.headers.get('Authorization')   
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()
    
        # Access the installedAppList array from the request data
        installedAppList = request.data.get('installedAppList', [])

        # Serialize the installedAppList to JSON
        json_data = json.dumps(installedAppList, indent=2)

        # Write the JSON data to a file
        with open('installedAppList.json', 'w') as json_file:
            json_file.write(json_data)

        accessToken = AccessToken(token)
        userId = accessToken.payload.get('userId')
        try:
            user_data = AppUsersModel.objects.get(userId=userId,isDeleted=0)

            app_entries = []  # Create a list to store the app entries

            # Now you can loop through the array and process the app data
            for app_data in installedAppList:
                packageName = app_data.get('packageName')

                # Check if an app with the given package name exists
                try:
                    app = AppMasterModel.objects.get(packageName=packageName,isDeleted=0)
                    # If it exists, update the app name and app logo
                    serializer = AppMasterSerializer(app, data=app_data, partial=True)
                    if serializer.is_valid():
                        serializer.save()
                        app_entries.append(serializer.data)
                    else:
                        response_data = {
                            "code": "400",
                            "message": "Invalid data",
                            "data": serializer.errors
                        }
                        return JsonResponse(response_data, status=status.HTTP_400_BAD_REQUEST)
                except AppMasterModel.DoesNotExist:
                    # If it doesn't exist, create a new app entry
                    serializer = AppMasterSerializer(data=app_data)

                    if serializer.is_valid():
                        serializer.save()
                        app_entries.append(serializer.data)
                    else:
                        response_data = {
                            "code": "400",
                            "message": "Invalid data",
                            "data": serializer.errors
                        }
                        return JsonResponse(response_data, status=status.HTTP_400_BAD_REQUEST)
                
            LOGGER.info(json.dumps("insert in applist"))    
            app_list_id = add_list_data(userId,app_entries,user_data.firstName)
                
            response_data = {
                "code": "201",
                "message": "Details saved successfully",
                "data": "null"
            }    

            return JsonResponse(response_data, status=status.HTTP_201_CREATED)
        except AppUsersModel.DoesNotExist:
            response_data = {
                "code": "404",
                "message": "User not found",
                "data": "null"
            }    
            return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND)    
    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)    


@swagger_auto_schema(method='post', operation_description="child installed apps", tags=['app-list'], request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'childId': openapi.Schema(type=openapi.TYPE_STRING, description='chidID')
    }
))
@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def get_app_list(request):
    token_key = request.headers.get('Authorization')
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()
        accessToken = AccessToken(token)
        userId = accessToken.payload.get('userId')
        app_data = AppListModel.objects.filter(userId=request.data['childId'],isDeleted=0).order_by('-id')
        serializer = AppListDisplaySerializer(app_data, many=True, context={"request": request})
        return JsonResponse({'message': 'User App List', 'data': serializer.data}, status=status.HTTP_200_OK)
        # return Response({'code':'200','message': 'User App List', 'data': serializer.data}, status=status.HTTP_200_OK)
    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    
@swagger_auto_schema(method='post', operation_description="child installed apps", tags=['app-list'], request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'childId': openapi.Schema(type=openapi.TYPE_STRING, description='chidID')
    }
))
@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def get_application_list(request):
    token_key = request.headers.get('Authorization')   
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()
        accessToken = AccessToken(token)
        userId = accessToken.payload.get('userId')
        app_data = AppListModel.objects.filter(userId=request.data['childId'],isDeleted=0).order_by('-id')
        serializer = AppListDisplaySerializer(app_data, many=True, context={"request": request})
        return JsonResponse({'code':'200','message': 'User App List', 'data': serializer.data}, status=status.HTTP_200_OK)
    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)     

@swagger_auto_schema(method='post', operation_description="pair with child", tags=['app-user'], request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'childCode': openapi.Schema(type=openapi.TYPE_STRING, description='pair')
    }
))
@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def pairing(request):
    token_key = request.headers.get('Authorization')   
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()
   
        accessToken = AccessToken(token)
        userId = accessToken.payload.get('userId')
        try:
            user = AppUsersModel.objects.get(userId=userId,isDeleted=0)
            if user.userType == 1:
                try:
                    get_code = PairingCodeModel.objects.get(code=request.data['childCode'],isDeleted=0)
                    LOGGER.info(json.dumps("In Try")) 
                except PairingCodeModel.DoesNotExist:
                    response_data = {
                        "code": "404",
                        "message": "Code is not found or incorrect",
                        "data": "null"
                    }
                    print("Response data",response_data)
                    log_data = {"Response data": response_data}
                    LOGGER.info(json.dumps(log_data)) 
                    return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND)

                # Check if a record already exists with the same code and connectionStatus=1
                existing_pair = ParentChildPairModel.objects.filter(childId=get_code.userId, connectionStatus=1).first()
                
                if existing_pair:
                    response_data = {
                        "code": "404",
                        "message": "Child is already paired",
                        "data": "null"
                    }
                    return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND) 
                    
                
                # Check if a record exists with the same code and connectionStatus=0
                existing_pair = ParentChildPairModel.objects.filter(code=get_code.code, parentId=userId, childId=get_code.userId, connectionStatus=0).first()

                if existing_pair:
                    # Update the existing record
                    existing_pair.connectionStatus = 1
                    existing_pair.isDeleted = 0
                    existing_pair.devicePairedTime = timezone.now()
                    existing_pair.disconnectedTime = None
                    existing_pair.save()
                    notification_service.notification_user(get_code.userId,'paired')    
                else:
                    # Create a new record
                    pair = ParentChildPairModel(code=get_code.code, parentId=userId, childId=get_code.userId)
                    pair.connectionStatus = 1
                    pair.devicePairedTime = timezone.now()
                    pair.disconnectedTime = None
                    pair.save()
                    notification_service.notification_user(get_code.userId,'paired')    
            else:
                response_data = {
                    "code": "404",
                    "message": "Details not found",
                    "data": "null"
                }
                return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND)     
            

            LOGGER.info(json.dumps("PAIR API")) 
            app_data = AppListModel.objects.filter(userId=get_code.userId,isDeleted=0).order_by('-id')
            serializer = AppListDisplaySerializer(app_data, many=True, context={"request": request})
            child_data = AppUsersModel.objects.get(userId=get_code.userId,isDeleted=0)
            is_connected = ParentChildPairModel.objects.filter(parentId=userId, childId=get_code.userId,connectionStatus=1,isDeleted=0).exists()

            response_data ={
                "code": "201",
                "message": "Paired Successfully",
                "data": {
                    "childInfo": {
                    "name": child_data.firstName,
                    "email": child_data.email,
                    "pairCode": get_code.code,
                    "userId": child_data.userId
                    },
                    "connectionInfo": {
                    "lastNotificationReceived": "null",
                    "devicePairedTime": existing_pair.devicePairedTime if existing_pair else pair.devicePairedTime,
                    "connectionStatus": 1 if is_connected else 0,
                    "disconnectedTime": "null"
                    },
                    "deviceInfo": {
                    "appVersion": child_data.appVersion,
                    "androidVersion": child_data.androidVersion,
                    "manufacturerName": child_data.manufacturerName,
                    "modelName": child_data.modelName
                    },
                    "installedAppList": serializer.data
                }
                }

            return JsonResponse(response_data, status=status.HTTP_201_CREATED)
        except AppUsersModel.DoesNotExist:
            response_data = {
                "code": "404",
                "message": "User not found",
                "data": "null"
            }    
            return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND) 
    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
     

@swagger_auto_schema(method='post', operation_description="unpair with child", tags=['app-user'], request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'paringCode': openapi.Schema(type=openapi.TYPE_STRING, description='unpair')
    }
))
@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def unpair(request):
    token_key = request.headers.get('Authorization')   
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()
    
        accessToken = AccessToken(token)
        userId = accessToken.payload.get('userId')
        try:
            user = AppUsersModel.objects.get(userId=userId,isDeleted=0)
            
            try:
                check_pair = ParentChildPairModel.objects.get(code=request.data['paringCode'],connectionStatus=1)
                check_pair.connectionStatus = 0
                check_pair.isDeleted = 1
                check_pair.disconnectedTime = timezone.now()
                check_pair.save()
                user_list = AppListModel.objects.filter(userId=check_pair.childId)
                for data in user_list:
                    data.isSelected = 1
                    data.save()
                notification_service.notification_user(check_pair.parentId,'unpair') 
                notification_service.notification_user(check_pair.childId,'unpair') 
                response_data = {
                    "code": "201",
                    "message": "Unpaired Successfully",
                    "data": "null"
                }
                return JsonResponse(response_data,status=status.HTTP_201_CREATED)

            except ParentChildPairModel.DoesNotExist:
                response_data = {
                    "code": "404",
                    "message": "Details not found",
                    "data": "null"
                }
                return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND) 
        except AppUsersModel.DoesNotExist:
            response_data = {
                "code": "404",
                "message": "User not found",
                "data": "null"
            }    
            return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND)     
    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
@swagger_auto_schema(method='post', operation_description="for select app list", tags=['app-list'],
                     request_body=openapi.Schema(
                        type=openapi.TYPE_OBJECT,
                        properties={
                            'childId': openapi.Schema(type=openapi.TYPE_STRING, description='chidID'),
                            'selectedApplication': openapi.Schema(
                                type=openapi.TYPE_ARRAY,
                                items=openapi.Schema(type=openapi.TYPE_OBJECT),
                            ),
                        }
                    )
                    )
@api_view(['POST'])
@authentication_classes([])
@permission_classes([]) 
def select_app_list(request):
    token_key = request.headers.get('Authorization')   
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()
    
        # Access the selectedApplication array from the request data
        selectedAppList = request.data.get('selectedApplication', [])

        accessToken = AccessToken(token)
        userId = accessToken.payload.get('userId')

        existing_selected_apps = AppListModel.objects.filter(userId=request.data['childId'],isDeleted=0,isSelected=0)

        # Mark existing selected apps as not selected (isSelected = 1)
        for data in existing_selected_apps:
            data.isSelected = 1
            data.save()

        # Now you can loop through the array and process the app data
        for app_data in selectedAppList:
            packageName = app_data.get('packageName')

            # Check if an app with the given package name exists
            try:
                app = AppMasterModel.objects.get(packageName=packageName,isDeleted=0)
                app_list = AppListModel.objects.get(userId=request.data['childId'],app__packageName=packageName,isDeleted=0)
                app_list.isSelected = 0
                app_list.save()
            except AppMasterModel.DoesNotExist:
                response_data = {
                    "code": "404",
                    "message": "Details not found",
                    "data": "null"
                }
                return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND) 
            except AppListModel.DoesNotExist:
                response_data = {
                    "code": "404",
                    "message": "Details not found",
                    "data": "null"
                }
                return JsonResponse(response_data, status=status.HTTP_404_NOT_FOUND) 
                
        response_data = {
            "code": "201",
            "message": "Sent Successfully",
            "data": "null"
        }    

        return JsonResponse(response_data, status=status.HTTP_201_CREATED)
    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)

@swagger_auto_schema(method='post', operation_description="child", tags=['app-list'], request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'childId': openapi.Schema(type=openapi.TYPE_STRING, description='chidID')
    }
))
@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def get_select_app_list(request):
    token_key = request.headers.get('Authorization')   
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()
   
        accessToken = AccessToken(token)
        userId = accessToken.payload.get('userId')
        data = AppListModel.objects.filter(userId=request.data['childId'],isDeleted=0,isSelected=0).order_by('-id')
        serializer = AppListDisplaySerializer(data, many=True, context={"request": request})

        response_data = {
            "selectedApplication": []
        }
        response_data_out = {
            "selectedApplication": []
        }

        nested_application_list = []
        temp_null_nested_application_list = []

        for item in serializer.data:
            package_name = item['app']['packageName']

            connected_children = ParentChildPairModel.objects.filter(parentId=userId,childId=request.data['childId']).last()
            notification_data = NotificationModel.objects.filter(childId=request.data['childId'],parentId=userId,packageName=package_name,lastNotificationSent__gte=connected_children.devicePairedTime).last()

            if notification_data:
                item['app']['date'] = notification_data.lastNotificationSent
                nested_application_list.append(item)
        
            else:
                item['app']['date'] = "null"
                temp_null_nested_application_list.append(item)
            
        response_data["selectedApplication"].append(nested_application_list)

        # Sort the 'selectedApplication' list by 'date' in descending order
        response_data["selectedApplication"] = sorted(response_data["selectedApplication"][0], key=lambda x: x['app']['date'] if x['app']['date'] != "null" else "", reverse=True)
        for null_items in temp_null_nested_application_list:
            response_data["selectedApplication"].append(null_items)
        response_data_out["selectedApplication"].append(response_data["selectedApplication"])    
        # response_data_out["selectedApplication"] = response_data["selectedApplication"]     
        
        return JsonResponse(response_data_out)
        # return JsonResponse({'code':'200','message': 'Selected App List', 'data': response_data_out},status=status.HTTP_200_OK)

    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
@swagger_auto_schema(method='post', operation_description="child", tags=['app-list'], request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'childId': openapi.Schema(type=openapi.TYPE_STRING, description='chidID')
    }
))
@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def get_selection_app_list(request):
    token_key = request.headers.get('Authorization')   
    if not token_key or not token_key.startswith('Bearer ') and not token_key.startswith('bearer '):
        response_data = {
            "code": "401",
            "message": "Authorization header missing",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)
    
    try:
        _, token = token_key.split()
   
        accessToken = AccessToken(token)
        userId = accessToken.payload.get('userId')
        data = AppListModel.objects.filter(userId=request.data['childId'],isDeleted=0,isSelected=0).order_by('-id')
        serializer = AppListDisplaySerializer(data, many=True, context={"request": request})

        response_data = {
            "selectedApplication": []
        }
        response_data_out = {
            "selectedApplication": []
        }

        nested_application_list = []
        temp_null_nested_application_list = []

        for item in serializer.data:
            package_name = item['app']['packageName']

            connected_children = ParentChildPairModel.objects.filter(parentId=userId,childId=request.data['childId']).last()
            notification_data = NotificationModel.objects.filter(childId=request.data['childId'],parentId=userId,packageName=package_name,lastNotificationSent__gte=connected_children.devicePairedTime).last()

            if notification_data:
                item['app']['date'] = notification_data.lastNotificationSent
                nested_application_list.append(item)
        
            else:
                item['app']['date'] = "null"
                temp_null_nested_application_list.append(item)
            
        response_data["selectedApplication"].append(nested_application_list)

        # Sort the 'selectedApplication' list by 'date' in descending order
        response_data["selectedApplication"] = sorted(response_data["selectedApplication"][0], key=lambda x: x['app']['date'] if x['app']['date'] != "null" else "", reverse=True)
        for null_items in temp_null_nested_application_list:
            response_data["selectedApplication"].append(null_items)    
        response_data_out["selectedApplication"] = response_data["selectedApplication"]     
        
        return JsonResponse({'code':'200','message': 'Selected App List', 'data': response_data_out},status=status.HTTP_200_OK)

    except ValueError:
        response_data = {
            "code": "401",
            "message": "Invalid Authorization header",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED) 
    except TokenError:
        response_data = {
            "code": "401",
            "message": "Token is invalid or expired",
            "data": "null"
        }
        return JsonResponse(response_data, status=status.HTTP_401_UNAUTHORIZED)    